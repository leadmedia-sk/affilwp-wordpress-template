<?php get_header(); ?>

  <main>
    <div class="container">
      <div class="content full-width">
          <div class="box">
            <div class="box-offset box-404">
              <h1>404</h1>
              <p><i></i> <?php _e('Oops! The Page you requested was not found!', 'affilwp'); ?></p>
              <a href="<?php echo get_bloginfo('url'); ?>" class="btn btn-black"><?php _e('Back to home', 'affilwp'); ?></a>
            </div><!-- /.box-offset -->
          </div><!-- /.box -->
        </div><!-- /.content -->
    </div>
  </main>

<?php get_footer(); ?>