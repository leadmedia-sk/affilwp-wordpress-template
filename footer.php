<footer class="footer">
	<div class="footer-links">
		<div class="container">
			<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
				<img src="<?php echo get_option('site_logo_inverse'); ?>" alt="<?php bloginfo('name'); ?>">
			</a>
			<?php
			$footer_text = get_option('footer_text');
			if (!empty($footer_text)) :
				?>
				<div class="footer-text">
					<?php echo $footer_text; ?>
				</div><!-- /.footer-text -->
			<?php endif; ?>


			<?php if (have_rows('socialne_media', 'option')) : ?>

				<div class="social-icons inverse-icons clearfix">

					<?php while (have_rows('socialne_media', 'option')) : the_row(); ?>

						<?php
						$icon_type = get_sub_field('typ_ikony');
						$profile_url = get_sub_field('odkaz_na_profil');

						$image_icon = '';
						$fa_icon = '';

						if ($icon_type == 'img') {
							$image_icon = get_sub_field('obrazok_ikona');
						} elseif ($icon_type == 'fa') {
							$fa_icon = get_sub_field('fa_ikona');
						}
						?>

						<a target="_blank" href="<?php echo $profile_url; ?>">
							<?php if ($icon_type == 'img') : ?>
								<img src="<?php echo $image_icon['url']; ?>" alt="<?php echo $image_icon['alt']; ?>"/>
							<?php endif; ?>

							<?php if ($icon_type == 'fa') : ?>
								<i class="fa <?php echo $fa_icon; ?>"></i>
							<?php endif; ?>
						</a>

					<?php endwhile; ?>

				</div><!-- /.social-icons -->

			<?php endif; ?>

		</div><!-- /.container -->
	</div><!-- /.footer-links -->

	<div class="footer-copy">
		<div class="container">
			© <?php echo date("Y"); ?> <?php
			$footerCopyright = get_option('footer_copyright');
			if (!empty($footerCopyright)) {
				echo $footerCopyright . "<br><br>";
			}
			call_user_func('wp_before_footer');
			?>
		</div><!-- end .container -->
	</div><!-- /.footer-copy -->
</footer>
</div><!-- end .page -->


<?php wp_footer(); ?>


</body>

</html>