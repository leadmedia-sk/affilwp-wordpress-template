<?php

/**
 * Advanced custom field init
 */
require_once( TEMPLATEPATH . '/includes/acf-init/acf-init.php' );

/**
 * Flush rewrite rules on activation / deactivation
 */
register_deactivation_hook( __FILE__, 'affilwp_flush_rewrite_rules' );
register_activation_hook( __FILE__, 'affilwp_flush_rewrite_rules' );
function affilwp_flush_rewrite_rules() {
    review_cpt_init();
    review_category();
    flush_rewrite_rules();
}



/**
 * Kirki customize
 */
include_once( dirname( __FILE__ ) . '/includes/framework/kirki/kirki.php' );

if ( ! function_exists( 'my_theme_kirki_update_url' ) ) {
    function my_theme_kirki_update_url( $config ) {
        $config['url_path'] = get_stylesheet_directory_uri() . '/includes/framework/kirki/';
        return $config;
    }
}
add_filter( 'kirki/config', 'my_theme_kirki_update_url' );

/**
 * Theme updater
 */
require_once(dirname(__FILE__) . '/includes/theme-updates/theme-update-checker.php');

$update_checker = new ThemeUpdateChecker(
	// Theme folder (slug)
	'affilwp',
	// Manifest (JSON) URL
	// ThemeUpdateChecker checks this file and reads the version. Then it decides
	// if it's OK to update or not.
	'https://bitbucket.org/leadmedia-sk/affilwp-wordpress-template/raw/master/theme-manifest.json'
);


/**
 * Maintenance mode
 */
require_once( TEMPLATEPATH . '/includes/maintenance.php');


/**
 *Theme specifics
 */

// Include theme init
require_once( TEMPLATEPATH . '/includes/theme-init.php' );

// Include theme options 
require_once( TEMPLATEPATH . '/includes/options-page.php' );

// Include sidebar init
require_once( TEMPLATEPATH . '/includes/sidebar-init.php' );

// Include theme customizer
require_once( TEMPLATEPATH . '/includes/theme-customizer.php' );

// Include snippets and misc
require_once( TEMPLATEPATH . '/includes/snippets-and-misc.php' );

// Include plugin activation init
require_once( TEMPLATEPATH . '/includes/plugin-activation-init.php' );

// Include tiny mce init
require_once( TEMPLATEPATH . '/includes/tinymce-init.php' );

// Include gallery for inline content 
require_once( TEMPLATEPATH . '/includes/gallery.php' );

// Include opengraph init
require_once( TEMPLATEPATH . '/includes/opengraph-init.php' );

// Include better comments
require_once( TEMPLATEPATH . '/includes/comments.php' );


/**
 * Page templates metaboxes
 */

/**
 * Widgets
 */
require_once( TEMPLATEPATH . '/includes/widgets-init.php' );

/**
 * Custom post types
 */
// Reviews
require_once( TEMPLATEPATH . '/includes/cpt/reviews.php' );

// Reviews options
require_once( TEMPLATEPATH . '/includes/cpt/reviews-options.php' );

// Benefits for reviews
require_once( TEMPLATEPATH . '/includes/cpt/benefits.php' );

// Widget blocks
require_once( TEMPLATEPATH . '/includes/cpt/widget-blocks.php' );

/**
 * YASR module
 */
// Settings
require_once( TEMPLATEPATH . '/includes/yasr-settings.php' );



/**
 * Load demo content
 */
require_once( TEMPLATEPATH . '/includes/demo-content.php' );


