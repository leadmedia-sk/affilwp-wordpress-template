<?php get_header(); ?>

<?php 
$s=get_search_query();
$args = array(
    's' => $s,
    'post_type' => array('post', 'pages', 'reviews')
);

// The Query
$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) : ?>

  <main>
    <div class="container">
      <div class="content full-width">
          <div class="box">
            <div class="box-offset search-box">
              <h1><?php _e("Search results for:","affilwp"); ?>
                <span class="bg-warning query-search"><?php echo get_query_var('s') ?></span>
              </h1>
            </div><!-- /.box-offset -->
            <div class="box-offset">
              <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <ul class="list-unstyled">
                  <li class="border-box">
                    <h3>
                      <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                    <p>
                      <?php echo get_excerpt(250); ?>
                    </p>
                  </li>
                </ol>
              <?php endwhile; ?>
            </div><!-- /.box-offset -->
            <div class="page-nav">
              <?php pagenavi(); ?>
            </div>
          </div><!-- /.box -->
        </div><!-- /.content -->
    </div>
  </main>

  <?php else : ?>

  <main>
    <div class="container">
      <div class="content full-width">
          <div class="box">
            <div class="box-offset">
              <h1><?php _e("Nothing found","affilwp"); ?></h1>
              <div class="content-not-found">
                  <i class="icon icon-frown-o"></i>
                  <p>
                    <?php _e("Sorry, but nothing matched your search criteria. Please try again with some different keywords.","affilwp"); ?>
                  </p>
                </div>
            </div><!-- /.box-offset -->
          </div><!-- /.box -->
        </div><!-- /.content -->
    </div>
  </main>

  <?php endif; ?>
  
<?php get_footer(); ?>