<?php get_header(); ?>

<?php 
  // ACF get_field
  $top_reviews_display = get_field('zobrazit_top_recenzie');
?>

<?php if ($top_reviews_display === true) :?>
  <?php get_template_part('sections/top-products'); ?>
<?php endif; ?>

<?php if (have_posts()) : ?>  
  <main>
    <div class="container">
      <div class="content">
        <?php while (have_posts()) : the_post(); ?>

          <div class="box">
            <div class="box-offset detail-box">
              <h1><?php the_title(); ?></h1>

              <ul class="meta-info clearfix">
                <li><i class="icon-date"></i> <?php the_time('d'); ?>. <?php the_time('F'); ?> <?php the_time('Y'); ?></li>
                <li><i class="icon-views"></i> <?php if(function_exists('the_views')) { the_views(); } ?></li>
                <li><i class="icon-comments"></i> <a href="#comments" class="jsScrollLink"><?php printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'affilwp' ), number_format_i18n( get_comments_number() ) ); ?></a></li>
              </ul><!-- /.meta-info -->

              <div class="share-buttons-custom social-icons inverse-icons clearfix">
                <a id="facebook-share" data-text="<?php echo get_excerpt(60);?>" class="blank social-facebook"><i></i></a>
                <a id="googleplus-share" data-text="<?php echo get_excerpt(60);?>" class="blank social-google"><i></i></a>
                <a id="twitter-share" data-text="<?php echo get_excerpt(60);?>" class="blank social-twitter"><i></i></a>
              </div><!-- /.social-icons -->

            </div><!-- /.box-offset -->

            <?php get_template_part('parts/part-preview-image' ); ?>

            <?php get_template_part('parts/part-content'); ?>

            <?php if( have_rows('komponenty_clanku') ): ?>

              <?php while ( have_rows('komponenty_clanku') ) : the_row(); ?>

                <?php if( get_row_layout() == 'galeria' ): ?>
                  <?php get_template_part('parts/part-gallery'); ?>
                <?php endif; ?>

                <?php if( get_row_layout() == 'text' ): ?>
                  <?php get_template_part('parts/part-text'); ?>
                <?php endif; ?>

                <?php if( get_row_layout() == 'tabulka_plus_minus' ): ?>
                  <?php get_template_part('parts/part-advantages-disadvantages'); ?>
                <?php endif; ?>

                <?php if( get_row_layout() == 'tlacidlo' ): ?>
                  <?php get_template_part('parts/part-buttons'); ?>
                <?php endif; ?>

                <?php if( get_row_layout() == 'video' ): ?>
                  <?php get_template_part('parts/part-video'); ?>
                <?php endif; ?>

              <?php endwhile ?>

            <?php endif ?>

            <?php get_template_part('parts/part-similar-articles' ); ?>

            <?php get_template_part('parts/part-comments' ); ?>
            
          </div>


        <?php endwhile;?> 
      </div><!-- /.content -->

      <aside>
        <?php get_sidebar(); ?>
      </aside>
    </div><!-- /.container -->

    <a href="#top" class="scroll-top jsScrollLink"><i></i></a>
  </main>
<?php endif; ?>


<?php get_footer(); ?>