<?php get_header(); ?>


<?php if (have_posts()) : ?>  
  <main>
    <div class="container">
      <div class="content">
        <?php while (have_posts()) : the_post(); ?>

          <div class="box">
            <div class="box-offset detail-box">
              <h1><?php the_title(); ?></h1>

            </div><!-- /.box-offset -->

            <?php get_template_part('parts/part-content'); ?>
            
          </div>


        <?php endwhile;?> 
      </div><!-- /.content -->

      <aside>
        <?php get_sidebar(); ?>
      </aside>
    </div><!-- /.container -->

    <a href="#top" class="scroll-top jsScrollLink"><i></i></a>
  </main>
<?php endif; ?>


<?php get_footer(); ?>
