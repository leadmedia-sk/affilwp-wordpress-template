<?php if ( comments_open() ) : ?>
  <div class="box-offset">
    <?php comments_template( '', true ); ?>
  </div>
<?php endif; // End if comments_open() ?>