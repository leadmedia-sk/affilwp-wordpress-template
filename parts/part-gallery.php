<?php 

  /*
  Part Name: Galéria
  Description: galéria obrázkov
  */

  // ACF get_sub_field
  $images = get_sub_field('obrazky');
?>

<div class="box-offset gallery clearfix">
  <?php if( $images ): ?>
    <?php foreach( $images as $image ): ?>
      <a href="<?php echo $image['url']; ?>" class="fancy" data-fancybox-group="gallery">
        <img src="<?php echo $image['sizes']['medium']; ?>" width="200" height="134" alt="<?php echo $image['alt']; ?>" />
      </a>
    <?php endforeach; ?> 
  <?php endif ?>
</div><!-- /.gallery -->