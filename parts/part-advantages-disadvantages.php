<?php 

  /*
  Part Name: Tabuľka kladov a záporov (+/-)
  Description: 
  */

  // ACF get_sub_field
  $advantages_title = get_sub_field('oznacenie_kladov');
  $disadvantages_title = get_sub_field('oznacenie_zaporov');
  $advantages = get_sub_field('klady');
  $disadvantages = get_sub_field('zapory');

?>

<div class="box-offset">
  <div class="row cols-advantages">
    <?php if ($advantages) :?>
    <div class="col-sm-6">
      <div class="box-advantages">
        <?php if ($advantages_title) : ?>
          <h4><?php echo $advantages_title; ?></h4>
        <?php endif ?>
        <?php if( have_rows('klady') ): ?>
          <ul>                     
            <?php while( have_rows('klady') ): the_row(); 
              $advantage_text = get_sub_field('text');
            ?>
              <li>
                <?php echo $advantage_text; ?>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div><!-- /.box-advantages -->
    </div>
    <?php endif; ?>
    <?php if ($disadvantages):?>
    <div class="col-sm-6">
      <div class="box-disadvantages">
        <?php if ($disadvantages_title) :?>
          <h4><?php echo $disadvantages_title; ?></h4>
        <?php endif ?>
        <?php if( have_rows('zapory') ): ?>
          <ul>                     
            <?php while( have_rows('zapory') ): the_row(); 
              $disadvantage_text = get_sub_field('text');
            ?>
              <li>
                <?php echo $disadvantage_text; ?>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div><!-- /.box-advantages -->
    </div>
    <?php endif; ?>
  </div><!-- /.cols-advantages -->
</div>