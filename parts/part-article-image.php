<?php 
  // ACF get field
  $preview_image = get_field('nahladovy_obrazok');
?>
<?php if( $preview_image ) : ?>
  <a href="<?php echo get_permalink();?>" class="product-img">
    <img src="<?php echo $preview_image['url']; ?>" title="<?php echo $preview_image['alt']; ?>" />
    <?php if ($label_display === true) :?>
      <?php get_template_part('parts/part-label'); ?>
    <?php endif; ?>
  </a>
<?php endif; ?>