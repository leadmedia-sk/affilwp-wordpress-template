<?php 

  /*
  Part Name: Text
  Description: štandardný obsah, citácia, ohraničený box v rámčeku
  */

  // ACF get_sub_field
  $content = get_sub_field('obsah');
  $content_settings = get_sub_field('nastavenia_obsahu');
  
  $blockquote_author = '';
  if ($content_settings == 'blockquote_box') {
    $blockquote_author = get_sub_field('autor_citacie');
  }
?>

<?php if ($content_settings == 'default') : ?>
  <div class="box-offset">
    <?php echo $content; ?>
  </div>
<?php endif; ?>

<?php if ($content_settings == 'blockquote_box') : ?>
  <div class="box-offset">
    <blockquote>
      <p>
        <?php echo $content; ?>
      </p>
      <cite><?php echo $blockquote_author; ?></cite>
    </blockquote>
  </div>
<?php endif; ?>

<?php if ($content_settings == 'bordered_box') : ?>
  <div class="box-offset">
    <div class="border-box">
      <p>
        <?php echo $content; ?>
      </p>
    </div>
  </div>
<?php endif; ?>

