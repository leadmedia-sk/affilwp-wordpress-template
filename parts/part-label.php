<?php 

  /*
  Part Name: Informačný štítok
  Description: 
  */

?>

<?php if( have_rows('nastavenia_stitku') ): ?>

  <?php while( have_rows('nastavenia_stitku') ): the_row(); 
    // ACF get_sub_field
    $label_color = get_sub_field('farba_stitku');
    $label_text = get_sub_field('text_stitku');
  ?>

    <span class="label" style="background-color: <?php echo $label_color; ?>;"><?php echo $label_text; ?></span>

  <?php endwhile; ?>


<?php endif; ?>