<?php 
  
  /*
  Part Name: Náhľad obrázku
  Description: náhľad veľkej fotografie
  */

  // ACF get field
  $preview_image = get_field('nahladovy_obrazok');

?>

<?php if( $preview_image ) :?>
  <div class="box-offset detail-img">
    <a href="<?php echo $preview_image['url']; ?>" class="fancy" data-fancybox-group="gallery">
        <img src="<?php echo $preview_image['url']; ?>" title="<?php echo $preview_image['alt']; ?>" />
      <?php get_template_part('parts/part-label' ); ?>
    </a>
  </div><!-- /.detail-img -->
<?php endif; ?>