<?php 

  /*
  Part Name: Tlačidlo čítaj viac
  Description: 
  */

  // ACF get field
  $read_more = get_field('tlacidlo_citaj_viac');
  
?>

<a href="<?php echo get_permalink(); ?>" class="btn btn-primary btn-block <?php echo $btn_size; ?>">
  <?php if (!empty($read_more)) :?>
    <?php echo $read_more; ?>
  <?php else :?>
    <?php _e('Read more', 'affilwp'); ?>
  <?php endif; ?>
</a>