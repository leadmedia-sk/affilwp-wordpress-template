<?php 

  /*
  Part Name: Video Oembed
  Description: 
  */

  // ACF get_sub_field
  $embed_video = get_sub_field('video_url');

?>

<div class="box-offset">
  <?php if($embed_video) :?>
    <div class="video-box">
      <?php echo $embed_video; ?>
    </div><!-- /.video-box -->
  <?php endif; ?>
</div>