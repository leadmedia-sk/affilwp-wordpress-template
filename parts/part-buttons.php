<?php 

  /*
  Part Name: Tlačidlá
  Description: tlačidlo ako komponent recenzie
  */

  // ACF get_sub_field
  $buttons_align = get_sub_field('zarovnanie_tlacidiel');
  $buttons = get_sub_field('tlacidla');

?>

<div class="box-offset">
  <?php if($buttons) :?>
    <?php if( have_rows('tlacidla') ): ?>  
      <div class="text-<?php echo $buttons_align;?>">                  
        <?php while( have_rows('tlacidla') ): the_row(); 
          $button_text = get_sub_field('text_tlacidla');
          $button_color = get_sub_field('farba_tlacidla');
          $button_size_select = get_sub_field('velkost_tlacidla');
          $button_display = get_sub_field('pozicia_tlacidla');
          $button_url_type = get_sub_field('odkaz_tlacidla');
          $button_url_target_type = get_sub_field('ciel_odkazu');

          $button_url = '';
          if ($button_url_type == 'external_url') {
            $button_url = get_sub_field('externy_odkaz');
          }
          elseif ($button_url_type == 'internal_url') {
            $button_url = get_sub_field('interny_odkaz');
          }

          $button_url_target = '';
          if ($button_url_target_type == 'self') {
            $button_url_target = '_self';
          }
          elseif ($button_url_target_type == 'blank') {
            $button_url_target = '_blank';
          }

          $button_size = '';
          if ($button_size_select == 'xs') {
            $button_size = 'btn-xs';
          }
          elseif ($button_size_select == 'sm') {
            $button_size = 'btn-sm';
          }
          elseif ($button_size_select == 'md') {
            $button_size = 'btn-md';
          }
          elseif ($button_size_select == 'lg') {
            $button_size = 'btn-lg';
          }
        ?>
        
          <a target="<?php echo $button_url_target; ?>" href="<?php echo $button_url; ?>" class="btn <?php echo $button_size;?> btn-primary btn-border-bottom" style="background-color:<?php echo $button_color; ?>;border-color:<?php echo darken_color($button_color,2); ?>;"><?php echo $button_text; ?></a> <?php if ($button_display=='block'){echo '<br><br>';}; ?>
          <?php endwhile; ?>
      </div>
    <?php endif; ?>
    
      
    
  <?php endif; ?>
</div>