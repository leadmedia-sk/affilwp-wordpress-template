<?php 

  /*
  Part Name: Zoznam výhod
  Description: zoznam výhod v podobe ikon + tooltip textu
  */

  // ACF get_sub_field
  $benefits = get_sub_field('zoznam_vyhod');

?>

<?php if ($benefits) : ?>
  <div class="product-box-icons clearfix <?php echo $text_align; ?>">
    <?php foreach( $benefits as $post): // variable must be called $post (IMPORTANT) ?>
      <?php setup_postdata($post); ?>

        <?php 
          $icon_type = get_field('typ_ikony');
          $tooltip = get_field('tooltip_text');
          $tooltip_position = get_field('tooltip_pozicia');
          $image_icon = '';
          $fa_icon = '';

          if ($icon_type == 'img') {
            $image_icon = get_field('obrazok__ikona');
          } 
          elseif ($icon_type == 'fa') {
            $fa_icon = get_field('fa_ikona');
          }
        ?>
        
        <?php if ($icon_type == 'img') : ?>
          <img data-toggle="tooltip" data-placement="<?php echo $tooltip_position; ?>" title="<?php echo $tooltip; ?>" src="<?php echo $image_icon['url']; ?>" alt="<?php echo $image_icon['alt']; ?>" />
        <?php endif; ?>

        <?php if ($icon_type == 'fa') : ?>
          <i class="fa <?php echo $fa_icon; ?>" data-toggle="tooltip" data-placement="<?php echo $tooltip_position; ?>" title="<?php echo $tooltip; ?>"> </i>
        <?php endif; ?>
  
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endforeach; ?>
  </div><!-- /.product-box-icons -->
<?php endif; ?>