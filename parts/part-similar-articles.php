<?php
  $orig_post = $post;
  global $post;
  $tags = wp_get_post_tags($post->ID);
  $post_type = get_post_type( $post->ID );

  if ($tags) {
      $tag_ids = array();
  foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
      $args=array(
          'post_type' => $post_type,
          'tag__in' => $tag_ids,
          'post__not_in' => array($post->ID),
          'posts_per_page'=>10, // Number of related posts to display.
          'caller_get_posts'=>1
      );

  $similar_reviews_query = new wp_query( $args ); 
?>

<?php if ( $similar_reviews_query->have_posts() ) : ?> 
  <div class="box-offset">
      <div class="articles-slider-box">
        <h4><?php _e( 'Similar articles', 'affilwp' ); ?></h4>

        <div class="articles-slider-out">
          <div class="articles-slider">
            <div class="articles-slider-in">
              
              <?php while( $similar_reviews_query->have_posts() ) { $similar_reviews_query->the_post(); ?>
                <div class="product-box product-box-sm">
                  <div class="product-box-info">
                    <div class="row">
                      <div class="col-md-5">
                      <?php 
                        // part options
                        $label_display = false;
                        include(locate_template('/parts/part-article-image.php')); 
                      ?>
                      </div>
                      <div class="col-md-7">
                        <h5 class="h4"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <?php if ( 'reviews' == get_post_type() ) :?>
                          <div class="stars">
                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                          </div>
                        <?php endif; ?>
                        <p><?php echo get_excerpt(60); ?></p>
                      </div>
                    </div>
                  </div><!-- /.product-box-info -->
                </div><!-- /.product-box-sm -->
              <?php } ?>                
              
            </div><!-- /.articles-slider-in -->
          
            <div class="slider-nav">
              <a href="#" class="slider-prev"></a>
              <a href="#" class="slider-next"></a>
            </div><!-- /.slider-nav -->
          </div><!-- /.articles-slider -->
        </div><!-- /.articles-slider-out -->
      </div><!-- /.articles-slider-box --> 
  </div>

<?php endif; ?>

<?php
  }
  $post = $orig_post;
  wp_reset_query();
?>