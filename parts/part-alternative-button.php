<?php 

  /*
  Part Name: Alternatívne tlačidlo
  Description: 
  */

  // ACF get field
  $alternative_buttons = get_field('tlacidlo_alternativne');
  
?>


  <?php if( have_rows('tlacidlo_alternativne') ): ?>
    <?php while( have_rows('tlacidlo_alternativne') ): the_row(); 
      // ACF get field
      $button_text = get_sub_field('text_tlacidla');
      $button_url_type = get_sub_field('odkaz_tlacidla');
      $button_url_target_type = get_sub_field('ciel_odkazu');

      $button_url = '';
      if ($button_url_type == 'external_url') {
        $button_url = get_sub_field('externy_odkaz');
      }
      elseif ($button_url_type == 'internal_url') {
        $button_url = get_sub_field('interny_odkaz');
      }
      
      $button_url_target = '';
      if ($button_url_target_type == 'blank') {
        $button_url_target = '_blank';
      }
      elseif ($button_url_target_type == 'self') {
        $button_url_target = '_self';
      }
    ?>

    <a target="<?php echo $button_url_target; ?>" href="<?php echo $button_url; ?>" class="btn btn-primary btn-block btn-border">
      <?php echo $button_text; ?>
    </a>

  <?php endwhile; ?>
<?php endif; ?>
