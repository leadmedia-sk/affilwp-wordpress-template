# AffilWP WordPress Theme
Aktuálna verzia témy: **1.0.4**

### Verzia 1.0.5
* added titles for images
* logo size automatically adjusting now


### Verzia 1.0.4
* added support for blog posts archive and category listing
* bugfix - wrong redirect for posts / reviews

### Verzia 1.0.3
* podpora automatických aktualizácií témy
* pridaný plugin **Symple Shortcodes v2.0.2** ([dokumentácia](http://wpexplorer-demos.com/symple-shortcodes/inserting-shortcodes/))
* rôzne opravy

### Verzia 1.0.2
* remove 3rd party "acf-link"

### Verzia 1.0.1
* vyladenie chybových hlášok z WP_debug
* prázdna strana po nainštalovaní šablóny (pridaný index archív)
* pridaná hláška ak nie sú žiadne príspevky

## Autori
* Front-End Development: Michal Litványi ([litvanyi.sk](http://litvanyi.sk))
* Back-End Development: [Lead Media, s.r.o.](http://www.leadmedia.sk)
* Graphic Design: Zdenko Chabada

## Dokumentácia
* Online dokumentácia: ([http://www.dognet.sk/affilwp/docs](http://www.dognet.sk/affilwp/docs) )