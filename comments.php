  <div id="comments"> 
  <?php if ( post_password_required() ) : ?>
    <p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'affilwp' ); ?></p>
  </div><!-- #comments -->
  <?php
      /*
       * Stop the rest of comments.php from being processed,
       * but don't kill the script entirely -- we still have
       * to fully load the template.
       */
      return;
    endif;
  ?>

  <?php // You can start editing here -- including this comment! ?>

  <?php if ( have_comments() ) : ?>
    <span id="comments-title" class="h3 border">
      <?php _e( 'Discussion', 'affilwp' ); ?>
    </span>

    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
    <nav id="comment-nav-above">
      <h3 class="assistive-text"><?php _e( 'Comment navigation', 'affilwp' ); ?></h3>
      <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'affilwp' ) ); ?></div>
      <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'affilwp' ) ); ?></div>
    </nav>
    <?php endif; // check for comment navigation ?>

    <ul class="comments-list">
      <?php
        /*
         * Loop through and list the comments. Tell wp_list_comments()
         * to use affilwp_comment() to format the comments.
         * If you want to overload this in a child theme then you can
         * define affilwp_comment() and that will be used instead.
         * See affilwp_comment() in affilwp/functions.php for more.
         */
        wp_list_comments( array( 'callback' => 'affilwp_comment' ) );
      ?>
    </ul>

    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
    <nav id="comment-nav-below">
      <h3 class="assistive-text"><?php _e( 'Comment navigation', 'affilwp' ); ?></h3>
      <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'affilwp' ) ); ?></div>
      <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'affilwp' ) ); ?></div>
    </nav>
    <?php endif; // check for comment navigation ?>

    <?php
    /*
     * If there are no comments and comments are closed, let's leave a little note, shall we?
     * But we only want the note on posts and pages that had comments in the first place.
     */
    if ( ! comments_open() && get_comments_number() ) : ?>
    <p class="no-comments"><?php _e( 'Comments are closed.' , 'affilwp' ); ?></p>
    <?php endif; ?>

  <?php endif; // have_comments() ?>

  <?php comment_form(array(
    'title_reply' => __('Add comment','affilwp'),
    'class' => 'comments-form',
    'class_submit' => 'btn btn-primary',
    'name_submit' => __('Submit comment','affilwp'),
    'comment_field' => '
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <textarea cols="50" rows="2" class="form-control" name="comment" placeholder="Comment"></textarea>
          </div>
        </div>
      </div>
    ',
    'fields' => '
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <input type="text" class="form-control" name="author" value="" placeholder="Name">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="email" value="" placeholder="E-mail">
        </div>
      </div>
    </div>
    '
  )); ?>

</div><!-- #comments -->
