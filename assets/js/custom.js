var offset_pos = new Array();

(function($) {
    $(function(){
        // share buttons
        // Share buttons inizialize configuration
        // Custom share buttons
        $('.share-buttons-custom #twitter-share').sharrre({
            share: {
                twitter: true
            },
            enableHover: false,
            enableTracking: true,
            template: '<i></i>',
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('twitter');
            }
        });
        $('.share-buttons-custom #facebook-share').sharrre({
            share: {
                facebook: true
            },
            enableHover: false,
            enableTracking: true,
            template: '<i></i>',
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('facebook');
            }
        });
        $('.share-buttons-custom #googleplus-share').sharrre({
            share: {
                googlePlus: true,
            },
            urlCurl: '../../plugins/sharrre/sharrre.php',
            enableHover: false,
            enableTracking: true,
            template: '<i></i>',
            click: function(api, options) {
                api.simulateClick();
                api.openPopup('googlePlus');
            }
        });
        // target blank
        $('a.blank').attr('target', '_blank');
        // fancybox
        $('.fancy').fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            loop: false,
            helpers: {
                title: {
                    type: 'inside'
                },
                overlay: {
                    locked: false
                }
            }
        });
        $('.popup').fancybox({
            type: 'ajax',
            autoSize: true,
            fitToView: false,
            closeEffect: 'none',
            padding: 0,
            scrolling: 'no',
            arrows: false,
            wrapCSS: 'fancy-popup',
            afterShow: function() {
                valideForm("form.validate-popup");
            }
        });
        //tooltip
        $('[data-toggle=tooltip]').tooltip();
        //scroll link
        $('.jsScrollLink').click(function() {
            $('html,body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 500);
            return false;
        });
        // validation form
        valideForm("form.validate");
        valideForm("form.validate-reservation");
        // bootstrap navigation
        $('.navbar-nav li, .nav-pills li').each(function() {
            if ($(this).find('ul').size() > 0) {
                $(this).addClass('dropdown');
                $(this).children('a').addClass('dropdown-toggle').attr('data-toggle', 'dropdown').append('<span class="caret"></span>');
                $(this).children('ul').addClass('dropdown-menu');
            }
        });
        //navigation hover
        $('.navbar-nav li.dropdown, .nav-pills li.dropdown').mouseenter(function() {
            if (!$('.navbar-toggle').is(':visible')) {
                $(this).addClass('open');
            }
        }).mouseleave(function() {
            if (!$('.navbar-toggle').is(':visible')) {
                $(this).removeClass('open');
            }
        });
        $('body').delegate('.dropdown-toggle', 'click', function() {
            if (!$('.navbar-toggle').is(':visible')) {
                return false;
            }
        });
        //articles slider
        var slider_move = 0,
            max_move = Math.round($('.articles-slider-in .product-box').size() / 2);
        $('.slider-prev').hide();
        if (max_move < 2) {
            $('.slider-next').hide();
        }
        $('body').delegate('.slider-nav a', 'click', function() {
            if ($(this).hasClass('slider-next')) {
                slider_move++;
            } else {
                slider_move--;
            }
            $(".articles-slider-in").animate({
                marginLeft: slider_move * $('.articles-slider-in .product-box').outerWidth() * -2 + "px"
            }, 1000);
            if (slider_move + 1 < max_move) {
                $('.slider-next').show();
            } else {
                $('.slider-next').hide();
            }
            if (slider_move <= 0) {
                $('.slider-prev').hide();
            } else {
                $('.slider-prev').show();
            }
            return false;
        });
        //colorizer
        $('body').delegate('.jsBtnSettings', 'click', function() {
            if ($('.settings-box').hasClass('open-settings')) {
                $('.settings-box').removeClass('open-settings');
            } else {
                $('.settings-box').addClass('open-settings');
            }
            return false;
        });

        // Masonry init
        var $grid = $('.gallery').imagesLoaded( function() {
          // init Masonry after all images have loaded
          $grid.masonry({
            itemSelector: '.fancy'
          });
        });
    });
})(jQuery);

$(window).scroll(function() {
    if ($(window).scrollTop() > 0) {
        $('body').addClass('scroll');
    } else {
        $('body').removeClass('scroll');
    }
});

function valideForm(handler) {
    jQuery.validator.addMethod("default-value", function(value, element) {
        return value != element.defaultValue;
    }, "");
    jQuery.validator.messages.required = "";
    $(handler).validate({
        onkeyup: false
    });
}