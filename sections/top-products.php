<?php 

  // ACF get_sub_field
$top3_reviews_title = get_field('top_3_titulok', 'option');
$top3_reviews = get_field('top_3_recenzie', 'option', false);
?>


<?php if ($top3_reviews) :?>
<div class="jumbotron">
    <div class="container">
      <span class="h1 jumbotron-title">
        <?php if($top3_reviews_title): ?>
          <?php echo $top3_reviews_title; ?>
        <?php endif ?>
      </span>
      

      <div class="content">
        <?php 
        $top1_query = new WP_Query(array(
          'post_type'  => 'reviews',
          'posts_per_page'  => 1,
          'post__in'   => $top3_reviews,
          'orderby' => 'post__in'
          ));

        $top1_review = array();

        while( $top1_query->have_posts() ) : $top1_query->the_post();

        $top1_review = get_the_ID();
        ?>
        <div class="product-box product-box-lg">
          <div class="product-box-info">
            <div class="row">
              <div class="col-md-7">
                <?php 
                    // part options
                $label_display = true;
                include(locate_template('/parts/part-article-image.php')); 
                ?>
                <div class="stars">
                  <?php echo do_shortcode('[yasr_overall_rating size="medium"]'); ?>
                </div>
              </div>
              <div class="col-md-5">
                <span class="h2"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></span>
                <p>
                  <?php echo get_excerpt(350); ?>
                </p>
                <?php 
                    // part options
                $btn_size = 'btn-md';
                include(locate_template('/parts/part-read-more-button.php')); 
                ?>
              </div>
            </div>
          </div><!-- /.product-box-info -->
        </div><!-- /.product-box-lg -->
      <?php endwhile; wp_reset_query();  ?>
    </div><!-- /.content -->

    

    <aside>
      <?php 
      $exclude_reviews[] = $top1_review;

      $top2_query = new WP_Query(array(
        'post_type'  => 'reviews',
        'posts_per_page'  => 1,
        'post__in'   => array_diff($top3_reviews, $exclude_reviews),
        'orderby' => 'post__in',
        ));

      $top2_review = array();

      while( $top2_query->have_posts() ) : $top2_query->the_post();

      $top2_review = get_the_ID();

      ?>
      <div class="product-box product-box-sm">
        <div class="product-box-info">
          <div class="row">
            <div class="col-md-6">
              <?php 
                  // part options
              $label_display = true;
              include(locate_template('/parts/part-article-image.php')); 
              ?>
              <div class="stars">
                <?php echo do_shortcode('[yasr_overall_rating size="medium"]'); ?>
              </div>
            </div>
            <div class="col-md-6">
              <span class="h4"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></span>
              <p>
                <?php echo get_excerpt(100); ?>
              </p>
              <?php get_template_part('parts/part-read-more-button'); ?>
            </div>
          </div>         
        </div><!-- /.product-box-info -->
      </div><!-- /.product-box-sm -->
    <?php endwhile; wp_reset_query(); ?>

    
    <?php 
    $exclude_reviews[] = $top2_review;

    $top3_query = new WP_Query(array(
      'post_type'  => 'reviews',
      'posts_per_page'  => 1,
      'post__in'   => array_diff($top3_reviews, $exclude_reviews),
      'post__not_in'   => array(24,29),
      'orderby' => 'post__in'
      ));

    while( $top3_query->have_posts() ) : $top3_query->the_post();

    ?>
    <div class="product-box product-box-sm">
      <div class="product-box-info">
        <div class="row">
          <div class="col-md-6">
            <?php 
                  // part options
            $label_display = true;
            include(locate_template('/parts/part-article-image.php')); 
            ?>
            <div class="stars">
              <?php echo do_shortcode('[yasr_overall_rating size="medium"]'); ?>
            </div>
          </div>
          <div class="col-md-6">
            <span class="h4"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></span>
            <p>
              <?php echo get_excerpt(100); ?>
            </p>
            <?php get_template_part('parts/part-read-more-button'); ?>
          </div>
        </div>
      </div><!-- /.product-box-info -->
    </div><!-- /.product-box-sm -->
  <?php endwhile; wp_reset_query(); ?>
  </aside>
  </div><!-- /.container -->
</div>

<?php endif; ?>
