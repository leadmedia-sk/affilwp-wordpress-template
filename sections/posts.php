<div class="box box-offset">
  <div class="product-box">
    <div class="product-box-info">
      <div class="row">
        <div class="col-md-5">
          <?php
          // part options
          $label_display = true;
          include(locate_template('/parts/part-article-image.php'));
          ?>
        </div>
        <div class="col-md-7">
          <h2 class="h4"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
          <p>
            <?php echo get_excerpt(350); ?>
          </p>
          <div class="row">
            <div class="col-sm-6">
              <?php get_template_part('parts/part-read-more-button'); ?>
            </div>
            <div class="col-sm-6">
              <?php get_template_part('parts/part-alternative-button'); ?>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.product-box-info -->
  </div><!-- /.product-box-lg -->
</div><!-- /.box -->