<?php
  
  // ACF get_field
  $review_categories = get_field('kategoria_recenzii');
  
  global $paged;
    if( get_query_var( 'paged' ) )
      $my_page = get_query_var( 'paged' );
    else {
      if( get_query_var( 'page' ) )
        $my_page = get_query_var( 'page' );
      else
        $my_page = 1;
      set_query_var( 'paged', $my_page );
      $paged = $my_page;
    }

  if ($review_categories) {
      // Post type args
      $args=array(
        'post_type' => 'reviews',
        'orderby' => 'date',
        'post_status' => 'publish',
        //'posts_per_page' => 5,
        'suppress_filters'  => false,
        'tax_query' => array(
              array(
                  'taxonomy' => 'review_category',
                  'field'    => 'term_id',
                  'terms'    => $review_categories,
              )
        ),
        'tag' => $tag,
        'paged'=> $my_page
        );
  } elseif (is_archive() ) {

    if (is_tag()) {
      $tag_slug = get_query_var('tag');
      // Post type args
      $args=array(
        'post_type' => 'reviews',
        'orderby' => 'date',
        'post_status' => 'publish',
        'tag' => $tag_slug,
        'paged'=> $my_page
      ); 
    }

    if (is_tax()) {
      $review_category_id = get_queried_object()->term_id;
      // Post type args
      $args=array(
        'post_type' => 'reviews',
        'orderby' => 'date',
        'post_status' => 'publish',
        //'posts_per_page' => 5,
        'suppress_filters'  => false,
        'tax_query' => array(
          array(
            'taxonomy' => 'review_category',
            'field' => 'id',
            'terms' => $review_category_id, // Where term_id of Term 1 is "1".
            
          )
        ),
        'paged'=> $my_page
      ); 
    }

  } 
  else {
      // Post type args
      $args=array(
        'post_type' => 'reviews',
        'orderby' => 'date',
        'post_status' => 'publish',
        //'posts_per_page' => 5,
        'suppress_filters'  => false,
        'paged'=> $my_page,
      ); 
  }
  


  // The Query
  $reviews_query = null;
  $reviews_query = new WP_Query($args); 

?>

<?php if ( $reviews_query->have_posts() ) : ?>

  <?php while ( $reviews_query->have_posts() ) : $reviews_query->the_post(); ?>

    <div class="box box-offset">
      <div class="product-box">
        <div class="product-box-info">
          <div class="row">
            <div class="col-md-5">
              <?php 
                // part options
                $label_display = true;
                include(locate_template('/parts/part-article-image.php')); 
              ?>
            </div>
            <div class="col-md-7">
              <h2 class="h4"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="stars">
                  <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                </div>
                <p>
                  <?php echo get_excerpt(350); ?>
                </p>
                <div class="row">
                  <div class="col-sm-6">
                    <?php get_template_part('parts/part-read-more-button'); ?>
                  </div>
                  <div class="col-sm-6">
                    <?php get_template_part('parts/part-alternative-button'); ?>
                  </div>
                </div>
            </div>
          </div>
        </div><!-- /.product-box-info -->

          <?php if( have_rows('komponenty_recenzie') ): ?>

            <?php while ( have_rows('komponenty_recenzie') ) : the_row(); ?>

              <?php if( get_row_layout() == 'vyhody' ): ?>
                <?php 
                  // part options
                  $text_align = 'text-left';
                  include(locate_template('/parts/part-benefits.php')); 
                ?>
              <?php endif; ?>

            <?php endwhile ?>

          <?php endif ?>

      </div><!-- /.product-box-lg -->
    </div><!-- /.box -->

  <?php endwhile; ?>

<?php else: ?>
  <div class="box box-offset">
    <p class="lead" style="margin-bottom:0;text-align: center;"><i class="fa fa-frown-o"></i> <?php _e('Zatiaľ sa tu nenachádzajú žiadné príspevky', 'affilwp') ?></p>
  </div>
  
<?php endif; wp_reset_query(); ?>

<?php wp_pagenavi( array( 'query' => $reviews_query ) ); ?>

