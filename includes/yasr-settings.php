<?php

// [yasr_top_highest_rated_custom items="10"]
function yasr_top_highest_rated_custom_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'items' => '10'
    ), $atts );

    return "items = {$a['items']}";
}

/* change yasr top ten html structure*/
add_shortcode ('yasr_top_highest_rated_custom', 'yasr_top_highest_rated_callback_custom');

function yasr_top_highest_rated_callback_custom () {

    global $wpdb;

    if (get_field('kategoria_recenzii')) {
        // ACF get_field
        $review_categories = get_field('kategoria_recenzii'); 
        $review_categories_ids = join(',',$review_categories);    
    } 
    
    // Ak niesu vybrate ziadne kategorie v nastaveniach archivu
    if (empty($review_categories)) {
        if (is_archive() ) {
            if (is_tag()) {
                $tag_slug = get_query_var('tag');
                $query_result = $wpdb->get_results("SELECT v.overall_rating, v.post_id
                                            FROM " . YASR_VOTES_TABLE . " AS v, $wpdb->posts AS p
                                            JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                                            JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                                            JOIN $wpdb->terms t ON (tt.term_id = t.term_id)
                                            WHERE  v.post_id = p.ID
                                            AND p.post_status = 'publish'
                                            AND tt.taxonomy = 'post_tag'
                                            AND t.slug = '$tag_slug'
                                            AND v.overall_rating > 0
                                            ORDER BY v.overall_rating DESC, v.id ASC LIMIT 10");
            }

            if (is_tax()) {
                $review_category_id = get_queried_object()->term_id;
                $query_result = $wpdb->get_results("SELECT v.overall_rating, v.post_id
                                            FROM " . YASR_VOTES_TABLE . " AS v, $wpdb->posts AS p
                                            JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                                            JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                                            JOIN $wpdb->terms t ON (tt.term_id = t.term_id)
                                            WHERE  v.post_id = p.ID
                                            AND p.post_status = 'publish'
                                            AND tt.taxonomy = 'review_category'
                                            AND t.term_id = '$review_category_id'
                                            AND v.overall_rating > 0
                                            ORDER BY v.overall_rating DESC, v.id ASC LIMIT 10");
            }
        }
        else {
            $query_result = $wpdb->get_results("SELECT v.overall_rating, v.post_id
                                            FROM " . YASR_VOTES_TABLE . " AS v, $wpdb->posts AS p
                                            WHERE  v.post_id = p.ID
                                            AND p.post_status = 'publish'
                                            AND v.overall_rating > 0
                                            ORDER BY v.overall_rating DESC, v.id ASC LIMIT 10");  
        } 

    } 
    // Ak su nastavene kategorie v nastaveniach archivu
    else if (!empty($review_categories)) {

        $query_result = $wpdb->get_results("SELECT v.overall_rating, v.post_id
                                            FROM " . YASR_VOTES_TABLE . " AS v, $wpdb->posts AS p
                                            JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)
                                            JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                                            JOIN $wpdb->terms t ON (tt.term_id = t.term_id)
                                            WHERE  v.post_id = p.ID 
                                            AND p.post_status = 'publish'
                                            AND v.overall_rating > 0
                                            AND tt.taxonomy = 'review_category'
                                            AND tt.term_id IN ( $review_categories_ids )
                                            GROUP BY p.ID
                                            ORDER BY v.overall_rating DESC, v.id ASC LIMIT 10");

    }

     

    

    if ($query_result) {

        $shortcode_html = "<table class=\"reviews-table\">";

        $counter = 0;

        foreach ($query_result as $result) {

            $counter++;

            $post_title = get_the_title($result->post_id);

            $link = get_permalink($result->post_id); //Get permalink from post it

            $shortcode_html .= "<tr>   

                                    <th>".$counter."</th>
                                    <td>
                                        <h5><a href=\"$link\">$post_title</a></h5>
                                        <div class=\"stars\">
                                            <div class=\"rateit small\" data-rateit-starwidth=\"16\" data-rateit-starheight=\"16\" data-rateit-value=\"$result->overall_rating\" data-rateit-step=\"0.1\" data-rateit-resetable=\"false\" data-rateit-readonly=\"true\"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <span class=\"btn-divider\">
                                            <a href=\"$link\" class=\"btn btn-block\">".__('Read', 'affilwp')."</a>
                                        </span>
                                    </td>
                                    
                                </tr>";


        } //End foreach

        $shortcode_html .= "</table>";

        return $shortcode_html;

    } //end if $query_result

    else {
        _e("You don't have any votes stored", 'yet-another-stars-rating');
    }

} //End function


/**
* Change yasr_visitor_multiset shortcode
*/

function yasr_visitor_multiset_callback_custom ( $atts ) {


    $ajax_nonce_visitor_multiset = wp_create_nonce( "yasr_nonce_insert_visitor_rating_multiset" );

    global $wpdb;

    // Attributes
    extract( shortcode_atts(
        array(
            'setid' => '0',
            'postid' => FALSE,
            'show_average' => FALSE
        ), $atts )
    );

    //If it's not specified use get_the_id
    if (!$postid) {

        $post_id = get_the_ID();

    }

    else {

        $post_id = $postid;

    }

    $cookiename = 'yasr_multi_visitor_cookie_' . $post_id . '_' . $setid;

    $image = YASR_IMG_DIR . "/loader.gif";

    $average_txt = __("Average", "yet-another-stars-rating");

    $loader_html = "<span class=\"yasr-loader-multiset-visitor\" id=\"yasr-loader-multiset-visitor-$post_id-$setid\" >&nbsp; " . __("Loading, please wait",'yet-another-stars-rating') . ' <img src=' .  "$image" .' title="yasr-loader" alt="yasr-loader"></span>';

    $button_html = "<input type=\"submit\" name=\"submit\" id=\"yasr-send-visitor-multiset-$post_id-$setid\" class=\"btn btn-primary\" value=\"" . __('Submit!', 'yet-another-stars-rating') . " \"  />";

    if (isset($_COOKIE[$cookiename])) {

            $button = "";
            $star_readonly = 'true';
            $span_message_content = __('Thank you for voting! ', 'yet-another-stars-rating');

    }

    else {

        //If user is not logged in
        if (!is_user_logged_in()) {

            if (YASR_ALLOWED_USER === 'allow_anonymous') {

                $button = $button_html;
                $star_readonly = 'false';
                $span_message_content = "";

            }

            elseif (YASR_ALLOWED_USER === 'logged_only') {

                $button = $button_html;
                $star_readonly = 'true';
                $span_message_content = __("You must sign in to vote", 'yet-another-stars-rating');;

            }


        } //End if user logged in

        //Is user is logged in
        else {

                $button = $button_html;
                $star_readonly = 'false';
                $span_message_content = "";

            }

    }

    $set_name_content = yasr_get_multi_set_visitor ($post_id, $setid);

    if ($set_name_content) {

        $multiset_vote_sum = 0;//avoid undefined variable
        $multiset_rows_number = 0;//avoid undefined variable

        $shortcode_html="<table class=\"yasr_table_multi_set_shortcode\">";

        foreach ($set_name_content as $set_content) {

            if($set_content->number_of_votes > 0) {

                $average_rating = $set_content->sum_votes / $set_content->number_of_votes;

                $average_rating = round($average_rating, 1);

            }

            else {

                $average_rating = 0;

            }

            $shortcode_html .=  "<tr>
                                    <td>
                                        <span class=\"yasr-multi-set-name-field\">$set_content->name </span>
                                    </td>
                                    <td>
                                        <div class=\"rateit yasr-visitor-multi-$post_id-$setid\" id=\"$set_content->id \" data-rateit-value=\"$average_rating\" data-rateit-step=\"1\" data-rateit-resetable=\"false\" data-rateit-readonly=\"$star_readonly\"></div>
                                        <span class=\"yasr-visitor-multiset-vote-count\">$set_content->number_of_votes</span>
                                    </td>
                                 </tr>";

             $multiset_vote_sum = $multiset_vote_sum + $average_rating;
             $multiset_rows_number++;

        } //End foreach

        if ($show_average !== FALSE && $show_average !=='no'|| $show_average===FALSE && YASR_MULTI_SHOW_AVERAGE !== 'no') {

            $multiset_average = $multiset_vote_sum / $multiset_rows_number;
            $multiset_average = round($multiset_average, 1);

            $shortcode_html .= "<tr>
                                    <td colspan=\"2\" class=\"yasr-multiset-average\">
                                        $average_txt<div class=\"rateit medium \" data-rateit-value=\"$multiset_average\" data-rateit-starwidth=\"24\" data-rateit-starheight=\"24\" data-rateit-step=\"0.1\" data-rateit-resetable=\"false\" data-rateit-readonly=\"true\"></div>
                                    </td>
                                </tr>";

        }

        $shortcode_html.="<tr>
                            <td colspan=\"2\" class=\"text-center\">
                                $button
                                $loader_html
                                <span class=\"yasr-visitor-multiset-message\">$span_message_content</span>
                            </td>
                        </tr>
                        </table>";
    }

    else {

        $set_name=$wpdb->get_results($wpdb->prepare("SELECT field_name AS name, field_id AS id
                    FROM " . YASR_MULTI_SET_FIELDS_TABLE . "
                    WHERE parent_set_id=%d
                    ORDER BY field_id ASC", $setid));


        $shortcode_html="<table class=\"yasr_table_multi_set_shortcode\">";

        foreach ($set_name as $set_content) {

            $shortcode_html .=  "<tr>
                                    <td>
                                        <span class=\"yasr-multi-set-name-field\">$set_content->name </span>
                                    </td>
                                    <td>
                                        <div class=\"rateit yasr-visitor-multi-$post_id-$setid\" id=\"$set_content->id\" data-rateit-value=\"0\" data-rateit-step=\"1\" data-rateit-resetable=\"false\" data-rateit-readonly=\"false\"></div>
                                        <span class=\"yasr-visitor-multiset-vote-count\"> 0 </span>
                                    </td>
                                 </tr>";


            //First time, initialize all fields to 0

            //Find the highest_id (it's not auto increment on  db due to gd star compatibility)
            $highest_id=$wpdb->get_var("SELECT id FROM " . YASR_MULTI_SET_VALUES_TABLE . " ORDER BY id DESC LIMIT 1 ");

            //highest id is 0 if data is empty
            if (!$highest_id) {
                $new_id=0;
            }

            $new_id=$highest_id + 1;

            $wpdb->replace(
                    YASR_MULTI_SET_VALUES_TABLE,
                    array (
                            'id'=>$new_id,
                            'post_id'=>$post_id,
                            'field_id'=>$set_content->id,
                            'set_type'=>$setid,
                            'number_of_votes' => 0,
                            'sum_votes' => 0
                            ),
                    array ("%d", "%d", "%d",  "%d", "%d", "%d")
                    );


        } //end foreach ($set_name as $set_content)


        if ($show_average !== FALSE && $show_average !=='no' || $show_average===FALSE && YASR_MULTI_SHOW_AVERAGE !== 'no') {

            $shortcode_html .= "<tr>
                <td colspan=\"2\" class=\"yasr-multiset-average\">
                   $average_txt<div class=\"rateit medium \" data-rateit-value=\"0\" data-rateit-starwidth=\"24\" data-rateit-starheight=\"24\" data-rateit-step=\"1\" data-rateit-resetable=\"false\" data-rateit-readonly=\"true\"></div>
                </td>
            </tr>";

        }

        $shortcode_html.="<tr>
                            <td colspan=\"2\">
                                $button
                                $loader_html
                                <span class=\"yasr-visitor-multiset-message\">$span_message_content</span>
                            </td>
                        </tr>
                        </table>";

        $shortcode_html.="</table>";

    }


    $var_post_id = json_encode($post_id);
    $var_set_id = json_encode($setid);
    $var_ajax_url = json_encode(admin_url('admin-ajax.php'));
    $var_ajax_nonce_visitor_multiset = json_encode($ajax_nonce_visitor_multiset);

    $javascript = "

        <script type=\"text/javascript\">

            document.addEventListener('DOMContentLoaded', function(event) {

                var postId = $var_post_id;
                var setType = $setid;
                var ajaxurl = $var_ajax_url;
                var nonce = $var_ajax_nonce_visitor_multiset;

                yasrVisitorsMultiSet (postId, setType, ajaxurl, nonce);

            });

        </script>
        ";

    return $shortcode_html . $javascript;


    return $shortcode_html;

}

add_shortcode ('yasr_visitor_multiset', 'yasr_visitor_multiset_callback_custom');


/**
 * YASR metabox only for "reviews"
 */
function hide_metaboxes() {
    $post_types = get_post_types( 
    array(
            'public' => true
        ),
        'objects'
    );

    if( ! empty( $post_types ) ) {
        foreach( $post_types as $type ) {
            $exclude = array( 'reviews');

            if( TRUE === in_array( $type->name, $exclude ) )
                continue;
            remove_meta_box( 'yasr_metabox_multiple_rating', $type->name, 'normal' );
            remove_meta_box( 'yasr_metabox_overall_rating', $type->name, 'side' );
        }
    }
}
add_action( 'do_meta_boxes', 'hide_metaboxes' );