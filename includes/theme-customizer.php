<?php

// Remove sanitazition when needed
function do_not_filter_anything( $value ) {
  return $value;
}

function my_customize_register( $wp_customize ) {

  Kirki::add_config( 'global_config', array(
    'capability'    => 'edit_theme_options',
    'option_type'   => 'option',
    'option_name'   => '',
    'sanitize_callback' => '',
  ) );


  

  /*******************************************
  Headline + slogan display
  ********************************************/
  // Site logo
  Kirki::add_field( 'global_config', array(
      'settings' => 'site_headline_display',
      'section'  => 'title_tagline',
      'default' => '1',
      'label' => __('Display headline and slogan in header?', 'affilwp'),
      'description' => __('hide / show', 'affilwp'),
      'type'     => 'switch', 'choices'     => array(
        'on'  => __( 'On', 'affilwp' ),
        'off' => __( 'Off', 'affilwp' ),
    )
  ) );

  /*******************************************
  Custom logo
  ********************************************/

  // Site logo
  Kirki::add_field( 'global_config', array(
      'settings' => 'site_logo',
      'section'  => 'title_tagline',
      'default' => get_template_directory_uri().'/assets/images/layout/logo_transparent.png',
      'label' => __('Site logo', 'affilwp'),
      'description' => __('used in etc. header', 'affilwp'),
      'type'     => 'image'
  ) );

  // Site logo - inverse
  Kirki::add_field( 'global_config', array(
      'settings' => 'site_logo_inverse',
      'section'  => 'title_tagline',
      'default' => get_template_directory_uri().'/assets/images/layout/logo_transparent_inverse.png',
      'label' => __('Site logo inverse', 'affilwp'),
      'description' => __('used in etc. footer', 'affilwp'),
      'type'     => 'image'
  ) );
  

  
  /*******************************************
  Color scheme
  ********************************************/

  Kirki::add_section( 'template_colors', array(
      'title' =>  __('Color Scheme', 'affilwp'),
      'description' => __( 'Customize your template colors', 'affilwp' ),
      'panel'          => '', // Not typically needed.
      'priority'       => 50,
      'capability'     => 'edit_theme_options',
      'theme_supports' => '', // Rarely needed.
  ) );

  // main color
  Kirki::add_field( 'global_config', array(
      'settings' => 'color_scheme_1',
      'section'  => 'template_colors',
      'default' => '#5ba835',
      'label' => __('Main Color', 'affilwp'),
      'description' => __('used as global template color', 'affilwp'),
      'type'     => 'color'
  ) );

  // main color - alt
  Kirki::add_field( 'global_config', array(
      'settings' => 'color_scheme_1_alt',
      'section'  => 'template_colors',
      'default' => '#41871e',
      'label' => __('Main Color Alternative', 'affilwp'),
      'description' => __('used as alternative color to global template color, etc. button:hover, link:hover', 'affilwp'),
      'type'     => 'color'
  ) );

  // secondary color 
  Kirki::add_field( 'global_config', array(
      'settings' => 'color_scheme_2',
      'section'  => 'template_colors',
      'default' => '#27282c',
      'label' => __('Secondary Color', 'affilwp'),
      'description' => __('used as secondary template color', 'affilwp'),
      'type'     => 'color'
  ) );

  // secondary color - alt
  Kirki::add_field( 'global_config', array(
      'settings' => 'color_scheme_2_alt',
      'section'  => 'template_colors',
      'default' => '#212225',
      'label' => __('Secondary Color Alternative', 'affilwp'),
      'description' => __('used as alternative color to secondary template color, etc. navigation, footer', 'affilwp'),
      'type'     => 'color'
  ) );

  // secondary color - alt - dark
  Kirki::add_field( 'global_config', array(
      'settings' => 'color_scheme_2_alt_dark',
      'section'  => 'template_colors',
      'default' => '#000',
      'label' => __('Secondary Color Alternative Darken', 'affilwp'),
      'description' => __('used as darken alternative color to secondary template color, etc. navigation links, footer copyright, ', 'affilwp'),
      'type'     => 'color'
  ) );

  /*******************************************
  Footer
  ********************************************/

  Kirki::add_section( 'footer_content', array(
      'title' =>  __('Footer content', 'affilwp'),
      'description' => __( 'Customize your content in footer', 'affilwp' ),
      'panel'          => '', // Not typically needed.
      'priority'       => 500,
      'capability'     => 'edit_theme_options',
      'theme_supports' => '', // Rarely needed.
  ) );

  // Footer text
  Kirki::add_field( 'global_config', array(
      'settings' => 'footer_text',
      'section'  => 'footer_content',
      'default' => '',
      'label' => __('Footer text', 'affilwp'),
      'description' => __('Content displayed in footer section', 'affilwp'),
      'type'     => 'editor',
      'sanitize_callback' => 'do_not_filter_anything',
  ) );

  // Footer copyright
  Kirki::add_field( 'global_config', array(
      'settings' => 'footer_copyright',
      'section'  => 'footer_content',
      'default' => '',
      'label' => __('Footer copyright', 'affilwp'),
      'description' => __('Text displayed in footer copyright section. "©" and year are already included.', 'affilwp'),
      'type'     => 'textarea',
      'help' => __('html tags are allowed, etc. strong, em, i', 'affilwp'),
      'sanitize_callback' => 'do_not_filter_anything',
  ) );


}
add_action( 'customize_register', 'my_customize_register' );


function my_customize_colors() {
  /**********************
  colors
  **********************/
  // main color
  $color_scheme_1 = get_option( 'color_scheme_1' );
   
  // main color - alt
  $color_scheme_1_alt = get_option( 'color_scheme_1_alt' );
   
  // secondary color
  $color_scheme_2 = get_option( 'color_scheme_2' );
   
  // secondary color - alt
  $color_scheme_2_alt = get_option( 'color_scheme_2_alt' );

  // secondary color - alt - dark
  $color_scheme_2_alt_dark = get_option( 'color_scheme_2_alt_dark' );
   


  /****************************************
  styling
  ****************************************/
  ?>
  <style>
   
  a,
  .color,
  .reviews-table .btn,
  .jumbotron .jumbotron-title {
    color: <?php echo $color_scheme_1; ?>;
  }

  .scroll-top,
  .bg-color,
  .btn.btn-primary {
    background-color: <?php echo $color_scheme_1; ?>;
  }

  header,
  .reviews-table tr.second-highlight-row td,
  .reviews-table tr.second-highlight-row th {
    border-color: <?php echo $color_scheme_1; ?>;
  }

  ul.category-list li:after, .cat-item:after {
    border-left-color: <?php echo $color_scheme_1; ?>;
  }

  a:hover,
  .reviews-table .btn:hover {
    color: <?php echo $color_scheme_1_alt; ?>;
  }

  .scroll-top,
  .reviews-table,
  .reviews-table tr.first-highlight-row td,
  .reviews-table tr.first-highlight-row th {
    border-color: <?php echo $color_scheme_1_alt; ?>;
  }

  .scroll-top:hover {
    background-color: <?php echo $color_scheme_1_alt; ?>;
  }

  .btn.btn-primary:focus,
  .btn.btn-primary:hover {
    background: <?php echo $color_scheme_1_alt; ?>;
    border-color: <?php echo $color_scheme_1_alt; ?>;
  }

  .btn-primary.btn-border-bottom {
    border-bottom-color: <?php echo $color_scheme_1_alt; ?>;
  }

  .btn.btn-border {
    color: <?php echo $color_scheme_1; ?>;
    border: 2px solid <?php echo $color_scheme_1; ?>;
  }

  .btn.btn-border:hover {
    color: <?php echo $color_scheme_1_alt; ?>;
  }
  
  header .navigation {
    background-color: <?php echo $color_scheme_2; ?>;
  }

  header .navbar nav > ul.nav > .open > a, header .navbar nav > ul.nav > li.active > a, header .navbar nav > ul.nav > li > a:focus, header .navbar nav > ul.nav > li > a:hover, header .navbar nav > ul.nav > li.current-menu-item > a, header .navbar nav > ul.nav > li.current_page_item > a {
    background: <?php echo $color_scheme_2_alt_dark; ?>;
  }

  footer {
    background-color: <?php echo $color_scheme_2; ?>;
  }

  footer .footer-copy {
    background-color: <?php echo $color_scheme_2_alt; ?>;
  }


   
  </style>
       
  <?php
}
add_action( 'wp_head', 'my_customize_colors' );