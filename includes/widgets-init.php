<?php
/**
 * Load custom widgets
 */

// reviews widget
require_once( TEMPLATEPATH . '/includes/widgets/top-reviews-widget.php' );

// video widget
require_once( TEMPLATEPATH . '/includes/widgets/video-widget.php' );

// socials media widget
require_once( TEMPLATEPATH . '/includes/widgets/socials-widget.php' );

// button widget
require_once( TEMPLATEPATH . '/includes/widgets/button-widget.php' );

// widget block
require_once( TEMPLATEPATH . '/includes/widgets/widget-block.php' );

// contacts
require_once( TEMPLATEPATH . '/includes/widgets/contacts-widget.php' );