<?php
if ( ! function_exists( 'affilwp_comment' ) ) :
function affilwp_comment( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  switch ( $comment->comment_type ) :
    case 'pingback' :
    case 'trackback' :
  ?>
  <li class="post pingback">
    <p><?php _e( 'Pingback:', 'affilwp' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'affilwp' ), '<span class="edit-link">', '</span>' ); ?></p>
  <?php
      break;
    default :
  ?>
  

  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
    
     
      <ul class="meta-info clearfix">
        <?php
          /* translators: 1: comment author, 2: date and time */
          printf( __( '%1$s %2$s', 'affilwp' ),
            sprintf( '<li class="comment-name"><i class="icon-comments"></i> %s</li>', get_comment_author_link() ),
            sprintf( '<li><a href="%1$s"><i class="icon-date"></i> <time datetime="%2$s">%3$s</time></a></li>',
              esc_url( get_comment_link( $comment->comment_ID ) ),
              get_comment_time( 'c' ),
              /* translators: 1: date, 2: time */
              sprintf( __( '%1$s at %2$s', 'affilwp' ), get_comment_date(), get_comment_time() )
            )
          );
        ?>
        <li class="reply">
          <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'affilwp' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </li><!-- .reply -->
      </ul>

      <div class="comment-text">
        <div class="comment-content">
          <?php comment_text(); ?>
        </div>
        <?php if ( $comment->comment_approved == '0' ) : ?>
          <em class="comment-awaiting-moderation text-info"><?php _e( 'Your comment is awaiting moderation.', 'affilwp' ); ?></em>
        <?php endif; ?>
      </div>

      <div class="comment-edit">
        <?php edit_comment_link( __( 'Edit', 'affilwp' ), '<span>', '</span>' ); ?>
      </div>
  
    <!-- #comment-## -->
  

  <?php
      break;
  endswitch;
}
endif; // ends check for affilwp_comment()