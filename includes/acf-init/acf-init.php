<?php 

// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
 
    // update path
    $path = get_stylesheet_directory() . '/includes/framework/advanced-custom-fields/';
    
    // return
    return $path;
    
}
 

// 2. customize ACF settings

// ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
function my_acf_settings_dir( $dir ) {
 
    // update path
    $dir = get_stylesheet_directory_uri() . '/includes/framework/advanced-custom-fields/';
    
    // return
    return $dir;
    
}

// ACF l10n_textdomain
add_filter('acf/settings/l10n_textdomain', 'my_acf_settings_l10n_textdomain', 10, 1);
function my_acf_settings_l10n_textdomain( $textdomain ) {
 
    return 'affilwp';
    
}
 

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');


// 4. Include ACF
include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields/acf.php' );

// also fields
include_once( get_stylesheet_directory() . '/includes/acf-init/acf-fields.php' );

// 5. Include ACF addons

  // Font-awesome
  include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields-addons/acf-font-awesome/acf-font-awesome.php' );

  // Accordion tabs
  include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields-addons/acf-accordion/acf-accordion.php' );

  // Columns
  include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields-addons/acf-column-field/acf-column.php' );

  // Widget area
  include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields-addons/acf-widget-area-field/acf-widget-area.php');

  // Widget area
  include_once( get_stylesheet_directory() . '/includes/framework/advanced-custom-fields-addons/acf-tooltip/acf-tooltip.php');