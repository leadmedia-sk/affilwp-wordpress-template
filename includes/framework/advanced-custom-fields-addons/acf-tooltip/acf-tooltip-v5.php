<?php

function my_enqueue() {
    wp_enqueue_script( 'acf_tooltip_script', get_stylesheet_directory_uri() . '/includes/framework/advanced-custom-fields-addons/acf-tooltip/js/acf-tooltip-v5.js', array('jquery-qtip') );
    wp_enqueue_style( 'acf_tooltip_css', get_stylesheet_directory_uri() . '/includes/framework/advanced-custom-fields-addons/acf-tooltip/css/acf-tooltip.css' );
    wp_enqueue_script( 'jquery-qtip', get_stylesheet_directory_uri() . '/includes/framework/advanced-custom-fields-addons/acf-tooltip/js/jquery.qtip.min.js' );
	wp_enqueue_style( 'jquery-qtip.js', get_stylesheet_directory_uri() . '/includes/framework/advanced-custom-fields-addons/acf-tooltip/css/jquery.qtip.min.css' );
}
add_action( 'acf/input/admin_head', 'my_enqueue' );

?>
