<?php
/**
* Change original gallery output
*/
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    $atts = shortcode_atts( array(
        'size' => 'thumbnail'
    ), $attr);

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'medium',
        'include' => '',
        'exclude' => ''
    ), $attr));

   
    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    $size_class = sanitize_html_class( $atts['size'] );
    

    // Here's your actual output, you may customize it to your need
    $output = "<div class=\"photogallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}\">\n";
    $output .= "<ul class=\"photogallery-inn\">\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $image_link = wp_get_attachment_image_src($id, 'full');
        $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
        $description = get_post_field('post_excerpt', $attachment->ID);
        $alt_text = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );

        $output .= "<li class=\"item\">\n";
        $output .= "$image_output";
        $output .= "<a href=\"{$image_link[0]}\" class=\"item-overlay\" data-lightbox-gallery=\"gallery\" data-lightbox-title=\"{$description}\">\n";
        $output .= "<div class=\"item-overlay-in\">\n";
        $output .= "<span class=\"icon-more\"><i class=\"icon icon-eye\"></i></span>\n";
        $output .= "<span class=\"item-desc\">{$description}</span>\n";
        $output .= "<span class=\"item-alt\">{$alt_text}</span>\n";
        $output .= "</div>\n";
        $output .= "</a>\n";
        $output .= "</li>\n";
    }

    $output .= "</ul>\n";
    $output .= "</div>\n";

    return $output;
}