<?php

/**
 * AffilWP theme options page
 */

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => __('AffilWP', 'affilwp'),
    'menu_title'  => __('AffilWP', 'affilwp'),
    'menu_slug'   => 'theme-options',
    'capability'  => 'edit_posts',
    //'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => __('Kontakty', 'affilwp'),
    'menu_title'  => __('Kontakty', 'affilwp'),
    'parent_slug' => 'theme-options',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => __('Kódy sledovania (analytics)', 'affilwp'),
    'menu_title'  => __('Kódy sledovania (analytics)', 'affilwp'),
    'parent_slug' => 'theme-options',
  ));
  
}