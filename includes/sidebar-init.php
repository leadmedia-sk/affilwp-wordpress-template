<?php 
function my_widgets_init() {

   // Sidebar
   // Location: sidebar
   register_sidebar(array(
      'name'               => __('Sidebar', 'affilwp'),
      'id'                 => __('sidebar', 'affilwp'),
      'description'   => __( 'Located at sidebar.', 'affilwp'),
      'before_widget' => '<div class="box">',
      'after_widget' => '</div>',
      'before_title' => '<span class="aside-title">',
      'after_title' => '</span>',
   ));


}
/** Register sidebars by running  */
add_action( 'widgets_init', 'my_widgets_init' );
?>