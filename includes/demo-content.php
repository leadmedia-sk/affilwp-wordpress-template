<?php
/**
 * Load predefined demo content
 */ 

function affilwp_import_demo_content() {
    return array(
        array(
            'import_file_name'           => 'Recenzie kosačiek',
            'import_file_url'            => 'https://www.dognet.sk/affilwp/demo_content/kosacky/demo-content.xml',
            //'import_widget_file_url'     => 'https://www.dognet.sk/affilwp/demo_content/kosacky/widgets.wie',
            'import_customizer_file_url' => 'https://www.dognet.sk/affilwp/demo_content/kosacky/customizer.dat',
            'import_preview_image_url'   => 'https://www.dognet.sk/affilwp/demo_content/kosacky/screenshot.jpg',
            'import_notice'              => __( '', 'affilwp' ),
        ),
        array(
            'import_file_name'           => ' ',
            'import_file_url'            => ' ',
            'import_customizer_file_url' => ' ',
            'import_preview_image_url'   => ' ',
            'import_notice'              => __( '', 'affilwp' ),
        ),
        // array(
        //     'import_file_name'           => 'Recenzie výživových doplnkov',
        //     'import_file_url'            => 'https://www.dognet.sk/affilwp/demo_content/vitamins/demo-content.xml',
        //     //'import_widget_file_url'     => 'https://www.dognet.sk/affilwp/demo_content/vitamins/widgets.wie',
        //     'import_customizer_file_url' => 'https://www.dognet.sk/affilwp/demo_content/vitamins/customizer.dat',
        //     'import_preview_image_url'   => 'https://www.dognet.sk/affilwp/demo_content/vitamins/screenshot.jpg',
        //     'import_notice'              => __( '', 'affilwp' ),
        // ),
    );

}
add_filter( 'pt-ocdi/import_files', 'affilwp_import_demo_content' );

/**
 * After import setup
 */ 
function reset_permalinks() {
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure( '/%postname%/' );
}
function affilwp_after_import_demo_content() {

    // Menus to assign after import.
    $main_menu   = get_term_by( 'name', 'Hlavné menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
        'navbar-nav'   => $main_menu->term_id,
    ));

    // Set Front Page from Reading Options
    $front_page = get_page_by_title( 'Úvodná stránka' );
        if(isset( $front_page ) && $front_page->ID) {
        update_option('show_on_front', 'page');
        update_option('page_on_front', $front_page->ID);
    }

    reset_permalinks();
}
add_action( 'pt-ocdi/after_import', 'affilwp_after_import_demo_content' );



/**
 * Change admin page
 */ 
function ocdi_plugin_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'themes.php';
    $default_settings['page_title']  = esc_html__( 'Inštalácia demo obsahu' , 'affilwp' );
    $default_settings['menu_title']  = esc_html__( 'Inštalácia demo obsahu' , 'affilwp' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'demo-import';

    return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'ocdi_plugin_page_setup' );