<?php

function doctype_opengraph($output) {
    return $output . '
    xmlns:og="http://opengraphprotocol.org/schema/"
    xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'doctype_opengraph');


function fb_opengraph() {
    global $post;
 
    if(is_single() || is_home() || is_page_template( 'homepage-template.php' )) {
        if(has_post_thumbnail($post->ID)) {
            $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium'); $img_src = $img_src[0];
        } elseif (get_field('nahladovy_obrazok')){
            // ACF get field
            $img_src = get_field('nahladovy_obrazok');
        }
        else {
            // $img_src = get_stylesheet_directory_uri() . '/img/opengraph_image.jpg';
            $img_src = get_option('site_logo');
        }
        if($excerpt = $post->post_excerpt) {
            $excerpt = strip_tags($post->post_excerpt);
            $excerpt = str_replace("", "'", $excerpt);
        } else {
            $excerpt = get_bloginfo('description');
        }
        ?>
 
    <meta property="og:title" content="<?php echo the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo $img_src; ?>"/>
 
<?php
    } else {
        return;
    }
}
add_action('wp_head', 'fb_opengraph', 5);

