<?php


/**
 * Custom read more link
 */
add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<a class="button button--primary" href="' . get_permalink() . '">'.  __('Read more','affilwp') .'</a>';
}

/**
 * Limit excerpt lenght and round it to nearest space.
 * Supports multibyte encoding (UTF8).
 *
 * @param int $limit Length of final excerpt
 * @param string $source Either 'content' or leave blank for excerpt
 * @return string
 */
function get_excerpt($limit, $source = NULL){
	$excerpt = (strtolower($source) === "content") ? get_the_content() : get_the_excerpt();

	$excerpt = mb_ereg_replace(" (\[.*?\])", "", $excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = mb_substr($excerpt, 0, $limit);
	$excerpt = mb_substr($excerpt, 0, mb_strripos($excerpt, " "));
	$excerpt = trim(mb_ereg_replace("/\s+/", " ", $excerpt));
	$excerpt = $excerpt . '<span class="excerptdots">...</span>';

	return $excerpt;
}


/**
 * HTML title
 */
function html_title()
{
    if ( is_home() )
    {
            bloginfo( 'name' );
            echo ' | ';
            bloginfo( 'description' );
    }
    else
    {
            bloginfo( 'name' );
            wp_title( '|', true, 'left' );
           
    }
}


/**
 * Change default login form to administration
 */ 
function my_login_style() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_option('site_logo'); ?>); 
            padding-bottom: 0px;
            background-size: 100%;
            width: 100%;
            height: 60px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_style' );



/**
 * odstranime zbytocnosti z dashboardu
 */
add_action( 'admin_menu', 'edit_admin_menus', 999 );
function edit_admin_menus()
{
    //remove_menu_page('edit.php'); //articles
}


/* Allow shortcodes in widget areas */
add_filter('widget_text', 'do_shortcode');

/**
 * Color function
 */
function darken_color($rgb, $darker=2) {

    $hash = (strpos($rgb, '#') !== false) ? '#' : '';
    $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
    if(strlen($rgb) != 6) return $hash.'000000';
    $darker = ($darker > 1) ? $darker : 1;

    list($R16,$G16,$B16) = str_split($rgb,2);

    $R = sprintf("%02X", floor(hexdec($R16)/$darker));
    $G = sprintf("%02X", floor(hexdec($G16)/$darker));
    $B = sprintf("%02X", floor(hexdec($B16)/$darker));

    return $hash.$R.$G.$B;
}


// Include the Google Analytics Tracking Code (ga.js)
// @ https://developers.google.com/analytics/devguides/collection/gajs/
if (get_field('google_analytics_kod','option')):

    function google_analytics_tracking_code(){
        $UA_ID = get_field('google_analytics_kod','option'); // GA Property ID
        ?>
            <script type="text/javascript">
              var _gaq = _gaq || [];
              _gaq.push(['_setAccount', '<?php echo $UA_ID; ?>']);
              _gaq.push(['_trackPageview']);

              (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
              })();
            </script>
    <?php 
    }

    $code_location = get_field('umiestnenie_kodu_sledovania','option');

    if ($code_location == 'header') :
        // include GA tracking code before the closing head tag
        add_action('wp_head', 'google_analytics_tracking_code');
    endif;

    // OR include GA tracking code before the closing body tag
    if ($code_location == 'footer') :
     add_action('wp_footer', 'google_analytics_tracking_code');
    endif;
endif;


/**
* wp_get_attachment function
*/
function wp_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

/**
* Show home in nav_menu
*/

function affil_wp_home_page_menu_args( $args ) {
    $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'affil_wp_home_page_menu_args' );


/**
 * This plugin will fix the problem where next/previous of page number buttons are broken on list
 * of posts in a category when the custom permalink string is:
 * /%category%/%postname%/ 
 * The problem is that with a url like this:
 * /categoryname/page/2
 * the 'page' looks like a post name, not the keyword "page"
 */
function remove_page_from_query_string($query_string)
{ 
    if (isset($query_string['name']) == 'page' && isset($query_string['page'])) {
        unset($query_string['name']);
        // 'page' in the query_string looks like '/2', so i'm spliting it out
        list($delim, $page_index) = split('/', $query_string['page']);
        $query_string['paged'] = $page_index;
    }      
    return $query_string;
}
// I will kill you if you remove this. I died two days for this line 
//add_filter('request', 'remove_page_from_query_string');

// following are code adapted from Custom Post Type Category Pagination Fix by jdantzer
function fix_category_pagination($qs){
    if( (isset($qs['category_name']) || isset($qs['author_name']) || isset($qs['tag']) ) && isset($qs['paged'])){
        $qs['post_type'] = get_post_types($args = array(
            'public'   => true,
            '_builtin' => false
        ));
        array_push($qs['post_type'],'post');
    }
    return $qs;
}
add_filter('request', 'fix_category_pagination');
