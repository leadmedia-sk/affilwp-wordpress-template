<?php

/*-----------------------------------------------------------------------------------*/
/*  Define Custom Post Type
/*-----------------------------------------------------------------------------------*/

// Post type: widget_blocks
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Widget blocks', 'affilwp'),
        'singular_name'         => __( 'Widget blocks', 'affilwp'),
        'menu_name'             => __( 'Widget blocks', 'affilwp'),
        'all_items'             => __( 'Widget blocks', 'affilwp'),
        'add_new'               => __( 'Add new', 'affilwp'),
        'add_new_item'          => __( 'Add new widget', 'affilwp'),
        'edit_item'             => __( 'Edit widget', 'affilwp'),
        'new_item'              => __( 'New widget', 'affilwp'),
        'view_item'             => __( 'Show widget', 'affilwp'),
        'search_items'          => __( 'Search widget', 'affilwp'),
        'not_found'             => __( 'Nothing found.', 'affilwp'),
        'not_found_in_trash'    => __( 'Nothing found in trash.', 'affilwp')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'menu_icon'             => '',
        'capability_type'       => 'page',
        'show_in_nav_menus'     => false,
        'show_in_menu' => 'themes.php',
        'supports'              => array('title', 'editor'),
        'rewrite' => array(
            // 'slug' => 'widget_blocks',
            // 'with_front' => false,
        )
        );
    register_post_type('widget_blocks', $args);
    flush_rewrite_rules();
});