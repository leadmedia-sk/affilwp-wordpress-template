<?php

/*-----------------------------------------------------------------------------------*/
/*  Define Custom Post Type
/*-----------------------------------------------------------------------------------*/

// Post type: Reviews
add_action('init', 'review_cpt_init');

function review_cpt_init() {
    $labels = array(
        'name'                  => __( 'Reviews', 'affilwp'),
        'singular_name'         => __( 'Review', 'affilwp'),
        'menu_name'             => __( 'Reviews', 'affilwp'),
        'all_items'             => __( 'All reviews', 'affilwp'),
        'add_new'               => __( 'Add review', 'affilwp'),
        'add_new_item'          => __( 'Add new review', 'affilwp'),
        'edit_item'             => __( 'Edit review', 'affilwp'),
        'new_item'              => __( 'New review', 'affilwp'),
        'view_item'             => __( 'Show review', 'affilwp'),
        'search_items'          => __( 'Search review', 'affilwp'),
        'not_found'             => __( 'Nothing found.', 'affilwp'),
        'not_found_in_trash'    => __( 'Nothing found in trash.', 'affilwp')
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'menu_icon'             => 'dashicons-star-filled',
        'menu_position'         => 5,
        'capability_type'       => 'page',
        'show_in_nav_menus'     => true,
        'supports'              => array('title', 'editor', 'comments')
    );
    register_post_type('reviews', $args);
}

// Register Custom Taxonomy
function review_category() {

    $labels = array(
        'name'                       => _x( 'Kategórie produktov', 'Taxonomy General Name', 'affilwp' ),
        'singular_name'              => _x( 'Kategória produktov', 'Taxonomy Singular Name', 'affilwp' ),
        'menu_name'                  => __( 'Kategórie produktov', 'affilwp' ),
        'all_items'                  => __( 'Všetky kategórie', 'affilwp' ),
        'parent_item'                => __( 'Parent Item', 'affilwp' ),
        'parent_item_colon'          => __( 'Parent Item:', 'affilwp' ),
        'new_item_name'              => __( 'Nová kategória', 'affilwp' ),
        'add_new_item'               => __( 'Pridať novú katregóriu', 'affilwp' ),
        'edit_item'                  => __( 'Upraviť kategóriu', 'affilwp' ),
        'update_item'                => __( 'Aktualizovať kategóriu', 'affilwp' ),
        'view_item'                  => __( 'Prezrieť kategóriu', 'affilwp' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'affilwp' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'affilwp' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'affilwp' ),
        'popular_items'              => __( 'Popular Items', 'affilwp' ),
        'search_items'               => __( 'Hľadať kategórie', 'affilwp' ),
        'not_found'                  => __( 'Nič nenájdené', 'affilwp' ),
        'no_terms'                   => __( 'Žiadne kategórie', 'affilwp' ),
        'items_list'                 => __( 'Zoznam kategórií', 'affilwp' ),
        'items_list_navigation'      => __( 'Items list navigation', 'affilwp' ),
    );

    $slug = get_field('slug_products_category','option');
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'rewrite'                    => array( 'slug' => $slug && $slug !== NULL   ? $slug : 'kategoria-produktov'),
    );
    register_taxonomy( 'review_category', array( 'reviews' ), $args );



}
add_action( 'init', 'review_category', 0 );


/**
 * Remove the slug from published post permalinks.
 */
function custom_remove_cpt_slug( $post_link, $post, $leavename ) {

    if ( 'reviews' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );  

/**
 * Some hackery to have WordPress match postname to any of our public post types
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * Typically core only accounts for posts and pages where the slug is /post-name/
 */
function custom_parse_request_tricksy( $query ) {

    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'reviews', 'page' ) );
    }
}
add_action( 'pre_get_posts', 'custom_parse_request_tricksy' );


/**
 * Remove the slug custom taxonomy
 */
add_filter('request', 'rudr_change_term_request', 1, 1 );
 
function rudr_change_term_request($request){

    $query = new WP_Query();  // the query isn't run if we don't pass any query vars
    $query->parse_query( $request );
 
    $tax_name = 'review_category'; // specify you taxonomy name here, it can be also 'category' or 'post_tag'
 
    // Request for child terms differs, we should make an additional check
    if( isset($request['attachment']) ) :
        $include_children = true;
        $name = $request['attachment'];
    else:
        $include_children = false;
        $name = isset($request['name']);
    endif;
 
 
    $term = get_term_by('slug', $name, $tax_name); // get the current term to make sure it exists
 
    if (isset($name) && $term && !is_wp_error($term)): // check it here
 
        if( $include_children ) {
            unset($query['attachment']);
            $parent = $term->parent;
            while( $parent ) {
                $parent_term = get_term( $parent, $tax_name);
                $name = $parent_term->slug . '/' . $name;
                $parent = $parent_term->parent;
            }
        } else {
            unset($request['name']);
        }
 
        switch( $tax_name ):
            case 'category':{
                $request['category_name'] = $name; // for categories
                break;
            }
            case 'post_tag':{
                $request['tag'] = $name; // for post tags
                break;
            }
            default:{
                $request[$tax_name] = $name; // for another taxonomies
                break;
            }
        endswitch;
 
    endif;
 
    return $request;
 
}
 
 
add_filter( 'term_link', 'rudr_term_permalink', 10, 3 );
 
function rudr_term_permalink( $url, $term, $taxonomy ){
 
    $taxonomy_name = 'review_category'; // your taxonomy name here
    $taxonomy_slug = 'review_category'; // the taxonomy slug can be different with the taxonomy name (like 'post_tag' and 'tag' )
 
    // exit the function if taxonomy slug is not in URL
    if ( strpos($url, $taxonomy_slug) === FALSE || $taxonomy != $taxonomy_name ) return $url;
 
    $url = str_replace('/' . $taxonomy_slug, '', $url);
 
    return $url;
}

add_action('template_redirect', 'rudr_old_term_redirect');
 
function rudr_old_term_redirect() {
 
    $taxonomy_name = 'review_category';
    $taxonomy_slug = 'review_category';
 
    // exit the redirect function if taxonomy slug is not in URL
    if( strpos( $_SERVER['REQUEST_URI'], $taxonomy_slug ) === FALSE)
        return;
 
    if( ( is_category() && $taxonomy_name=='category' ) || ( is_tag() && $taxonomy_name=='post_tag' ) || is_tax( $taxonomy_name ) ) :
 
            wp_redirect( site_url( str_replace($taxonomy_slug, '', $_SERVER['REQUEST_URI']) ), 301 );
        exit();
 
    endif;
 
}


/**
*
* Moving WP elements (such as the content editor) within ACF fields
*
**/
add_action('acf/input/admin_head', 'move_editor_to_review');

function move_editor_to_review() {
    
    ?>
    <script type="text/javascript">
    (function($) {
        
        $(document).ready(function(){
            
            $('.acf-field-56fa64dbf197b .acf-input').append( $('#postdivrich') );

            $(this).nextUntil(".acf-field-tab, .acf-field-accordion").removeClass('hidden-by-tab').wrapAll('<div class="acf-accordion-group"></div>');
            
        });
        
    })(jQuery);    
    </script>
    <style type="text/css">
        .acf-field #wp-content-editor-tools {
            background: transparent;
            padding-top: 0;
        }
    </style>
    <?php     
}


