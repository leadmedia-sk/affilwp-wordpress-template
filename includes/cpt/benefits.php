<?php

/*-----------------------------------------------------------------------------------*/
/*  Define Custom Post Type
/*-----------------------------------------------------------------------------------*/

// Post type: benefits
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Benefits', 'affilwp'),
        'singular_name'         => __( 'Benefits', 'affilwp'),
        'menu_name'             => __( 'Benefits', 'affilwp'),
        'all_items'             => __( 'Benefits', 'affilwp'),
        'add_new'               => __( 'Add new', 'affilwp'),
        'add_new_item'          => __( 'Add new benefit', 'affilwp'),
        'edit_item'             => __( 'Edit benefit', 'affilwp'),
        'new_item'              => __( 'New benefit', 'affilwp'),
        'view_item'             => __( 'Show benefit', 'affilwp'),
        'search_items'          => __( 'Search benefit', 'affilwp'),
        'not_found'             => __( 'Nothing found.', 'affilwp'),
        'not_found_in_trash'    => __( 'Nothing found in trash.', 'affilwp')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'menu_icon'             => '',
        'capability_type'       => 'page',
        'show_in_nav_menus'     => false,
        'show_in_menu' => 'edit.php?post_type=reviews',
        'supports'              => array('title'),
        'rewrite' => array(
            'slug' => 'benefits',
            'with_front' => false,
        )
        );
    register_post_type('benefits', $args);
    flush_rewrite_rules();
});



//Metaboxes
add_filter( 'cmb2_meta_boxes', 'benefits_metaboxes' );

function benefits_metaboxes( $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_benefit_';


    /**
     * Details
     */
    $benefit_metabox = new_cmb2_box( array(
        'id'           => 'benefit-detail',
        'title'        => __( 'Details', 'affilwp'),
        'object_types' => array( 'benefits' ), // post type
        'context'      => 'normal', //  'normal', 'advanced', or 'side'
        'priority'     => 'default',  //  'high', 'core', 'default' or 'low'
        'show_names'   => true, // Show field names on the left
    ) ); 

    // Image
    $benefit_metabox->add_field( array(
        'name' => __( 'Benefit image/icon', 'affilwp'),
        'description' => '',
        'type' => 'file',
        'id'   => $prefix .'image',
    ) );

    // Tooltip text
    $benefit_metabox->add_field( array(
        'name' => __( 'Benefit tooltip text', 'affilwp'),
        'description' => '',
        'type' => 'text',
        'id'   => $prefix .'tooltip',
    ) );


    // Add other metaboxes as needed

    return $meta_boxes;
}