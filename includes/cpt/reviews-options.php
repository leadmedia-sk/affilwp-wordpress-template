<?php 
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => __('Nastavenia recenzií', 'affilwp'),
    'menu_title'  => __('Nastavenia recenzií', 'affilwp'),
    'menu_slug'   => 'reviews-settings',
    'parent_slug' => 'edit.php?post_type=reviews',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  // acf_add_options_sub_page(array(
  //   'page_title'  => 'Readings',
  //   'menu_title'  => 'Readings',
  //   'parent_slug' => 'reviews-settings',
  // ));

  
}