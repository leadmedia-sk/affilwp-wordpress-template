<?php 
class affilwp_button_widget extends WP_Widget {

  // constructor
  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'affilwp_button_widget', 

    // Widget name will appear in UI
    __('AffilWP button', 'affilwp'), 

    // Widget description
    array( 'description' => __( 'Display button.', 'affilwp' ), ) 
    );
  }

  // widget form creation
  function form($instance) { 
  // Check values 
  if( $instance) { 
       $title = esc_attr($instance['title']); 
       $button_text = esc_attr($instance['button_text']);
       $button_url = esc_attr($instance['button_url']);
       $button_variant = esc_attr($instance['button_variant']);
       $button_size = esc_attr($instance['button_size']);
       $button_target = esc_attr($instance['button_target']); // Added 
  } else { 
       $title = ''; 
       $button_text = '';
       $button_url = '';
       $button_variant = '';
       $button_size = '';
       $button_target = ''; // Added 
  } 
  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e('Button text:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" type="text" value="<?php echo $button_text; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('button_url'); ?>"><?php _e('Button URL:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('button_url'); ?>" name="<?php echo $this->get_field_name('button_url'); ?>" type="url" value="<?php echo $button_url; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('button_target'); ?>"><?php _e('Button target:', 'affilwp'); ?></label>
    <select name="<?php echo $this->get_field_name('button_target'); ?>" id="<?php echo $this->get_field_id('button_target'); ?>" class="widefat">
    <?php
      $options = array('_blank', '_self');
      foreach ($options as $option) {
        echo '<option value="' . $option . '" id="' . $option . '"', $button_target == $option ? ' selected="selected"' : '', '>', $option, '</option>';
      }
      ?>
    </select>
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('button_size'); ?>"><?php _e('Button size:', 'affilwp'); ?></label>
    <select name="<?php echo $this->get_field_name('button_size'); ?>" id="<?php echo $this->get_field_id('button_size'); ?>" class="widefat">
    <?php
      $options = array('btn-lg', 'btn-sm', 'btn-md');
      foreach ($options as $option) {
        echo '<option value="' . $option . '" id="' . $option . '"', $button_size == $option ? ' selected="selected"' : '', '>', $option, '</option>';
      }
      ?>
    </select>
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('button_variant'); ?>"><?php _e('Button variant:', 'affilwp'); ?></label>
    <select name="<?php echo $this->get_field_name('button_variant'); ?>" id="<?php echo $this->get_field_id('button_variant'); ?>" class="widefat">
    <?php
      $options = array('btn-primary', 'btn-danger', 'btn-success', 'btn-info', 'btn-warning', 'btn-link' );
      foreach ($options as $option) {
        echo '<option value="' . $option . '" id="' . $option . '"', $button_variant == $option ? ' selected="selected"' : '', '>', $option, '</option>';
      }
      ?>
    </select>
  </p>
  <?php }

  // update widget
  function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['button_text'] = strip_tags($new_instance['button_text']);
        $instance['button_url'] = strip_tags($new_instance['button_url']);
        $instance['button_variant'] = strip_tags($new_instance['button_variant']);
        $instance['button_size'] = strip_tags($new_instance['button_size']);
        $instance['button_target'] = strip_tags($new_instance['button_target']);
       return $instance;
  }

  // display widget
  function widget($args, $instance) {
     extract( $args );
     // these are the widget options
     $title = apply_filters('widget_title', $instance['title']);
     $button_text = $instance['button_text'];
     $button_url = $instance['button_url'];
     $button_variant = $instance['button_variant'];
     $button_size = $instance['button_size'];
     $button_target = $instance['button_target'];
     echo $before_widget;
     // Display the widget
     echo '<div>';

     // Check if title is set
     if ( $title ) {
        echo $before_title . $title . $after_title;
     }

     // Check if button_text is set
     if( $button_text ) {
        echo '
        <a target="'.$button_target.'" href="'.$button_url.'" class="btn '.$button_variant.' '.$button_size.' btn-border-bottom btn-block">'.$button_text.'</a>
        ';
     }
     
     echo '</div>';
     echo $after_widget;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("affilwp_button_widget");'));
?>