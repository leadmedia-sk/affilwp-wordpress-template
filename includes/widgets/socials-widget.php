<?php 
class affilwp_socials_widget extends WP_Widget {

  // constructor
  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'affilwp_socials_widget', 

    // Widget name will appear in UI
    __('AffilWP socials media', 'affilwp'), 

    // Widget description
    array( 'description' => __( 'Display socials media links.', 'affilwp' ), ) 
    );
  }

  // widget form creation
  function form($instance) { 
  // Check values 
  if( $instance) { 
       $title = esc_attr($instance['title']); // Added 
  } else { 
       $title = ''; // Added 
  } 
  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  <?php }

  // update widget
  function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
       return $instance;
  }

  // display widget
  function widget($args, $instance) {
     extract( $args );
     // these are the widget options
     $title = apply_filters('widget_title', $instance['title']);

     echo $before_widget;

     // Display the widget

     // Check if title is set
     if ( $title ) {
        echo $before_title . $title . $after_title;
     }

     // check if the repeater field has rows of data
     if( have_rows('socialne_media', 'option') ) :

      // Display socials media links
      echo '
        <div class="social-icons clearfix">';

          // loop through the rows of data
          while ( have_rows('socialne_media', 'option') ) : the_row();

          // display a sub field value
          $icon_type = get_sub_field('typ_ikony');
          $profile_url = get_sub_field('odkaz_na_profil');

          $image_icon = '';
          $fa_icon = '';

          if ($icon_type == 'img') {
            $image_icon = get_sub_field('obrazok_ikona');
          } 
          elseif ($icon_type == 'fa') {
            $fa_icon = get_sub_field('fa_ikona');
          }

          // display repeater field

          echo '<a target="_blank" href="'.$profile_url.'">';

            if ($icon_type == 'img') :
              echo '<img src="'.$image_icon['url'].'" alt="'.$image_icon['alt'].'" />';
            endif; // if icon_type == img

            if ($icon_type == 'fa') :
              echo '<i class="fa '.$fa_icon.'"></i>';
            endif; // if icon_type == fa

          echo '</a>';



          endwhile; // while have_rows

       echo '</div><!-- /.social-icons -->';
       
      endif; //if have have_rows

     echo $after_widget;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("affilwp_socials_widget");'));
?>