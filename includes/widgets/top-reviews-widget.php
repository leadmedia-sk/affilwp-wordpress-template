<?php 
class affilwp_reviews_widget extends WP_Widget {

  // constructor
  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'affilwp_reviews_widget', 

    // Widget name will appear in UI
    __('AffilWP Top reviews', 'affilwp'), 

    // Widget description
    array( 'description' => __( 'Display Top highest rated reviews.', 'affilwp' ), ) 
    );
  }

  // widget form creation
  function form($instance) { 
  // Check values 
  if( $instance) { 
       $title = esc_attr($instance['title']); 
       $items = esc_attr($instance['items']); // Added 
  } else { 
       $title = ''; 
       $items = ''; // Added 
  } 
  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('items'); ?>"><?php _e('Number of items:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('items'); ?>" name="<?php echo $this->get_field_name('items'); ?>" type="text" value="<?php echo $items; ?>" />
  </p>
  <?php }

  // update widget
  function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['items'] = strip_tags($new_instance['items']);
       return $instance;
  }

  // display widget
  function widget($args, $instance) {
     extract( $args );
     // these are the widget options
     $title = apply_filters('widget_title', $instance['title']);
     $items = $instance['items'];
     echo $before_widget;
     // Display the widget
     echo '<div>';

     // Check if title is set
     if ( $title ) {
        echo $before_title . $title . $after_title;
     }

     // Check if items is set
     if( $items ) {
        echo do_shortcode('[yasr_top_highest_rated_custom items="'.$items.'"]');
     }
     
     echo '</div>';
     echo $after_widget;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("affilwp_reviews_widget");'));
?>