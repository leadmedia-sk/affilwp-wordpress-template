<?php 
class affilwp_widget_block_widget extends WP_Widget {

  // constructor
  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'affilwp_widget_block_widget', 

    // Widget name will appear in UI
    __('AffilWP widget block', 'affilwp'), 

    // Widget description
    array( 'description' => __( 'Display content of custom widget block.', 'affilwp' ), ) 
    );
  }

  // widget form creation
  function form($instance) { 
  // Check values 
  if( $instance) { 
       $title = esc_attr($instance['title']); 
       $widget_block = esc_attr($instance['widget_block']); // Added 
  } else { 
       $title = ''; 
       $widget_block = ''; // Added 
  } 
  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  
  
  <p>
    <label for="<?php echo $this->get_field_id('widget_block'); ?>"><?php _e('Widget block:', 'affilwp'); ?></label>
    <select name="<?php echo $this->get_field_name('widget_block'); ?>" id="<?php echo $this->get_field_id('widget_block'); ?>" class="widefat">
    <?php

      $args = wp_parse_args( $query_args, array(
          'post_type'   => 'widget_blocks',
          'numberposts' => -1,
      ) );

      $posts = get_posts( $args );

      $post_options = array();
      if ( $posts ) {
          foreach ( $posts as $post ) {
            $post_options[ $post->ID ] = $post->post_title;
            echo '<option value="' . $post->ID . '" id="' . $post->ID . '"', $widget_block == $post->ID ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
          }
      }

      ?>
    </select>
  </p>
  
  <?php }

  // update widget
  function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['widget_block'] = strip_tags($new_instance['widget_block']);
       return $instance;
  }

  // display widget
  function widget($args, $instance) {
     extract( $args );
     // these are the widget options
     $title = apply_filters('widget_title', $instance['title']);
     $widget_block = $instance['widget_block'];

     echo $before_widget;
     // Display the widget
     echo '<div>';

     // Check if title is set
     if ( $title ) {
        echo $before_title . $title . $after_title;
     }

     // Check if button_text is set
     if( $widget_block ) {
        $args = array(
            'post_type'   => 'widget_blocks',
            'numberposts' => 1,
            'p' => $widget_block
        ) ;

        // The Query
        $widget_block_query = null;
        $widget_block_query = new WP_Query($args); 
        if ( $widget_block_query->have_posts() ) {
          while ( $widget_block_query->have_posts() ) {
            $widget_block_query->the_post();
            the_content();
          } wp_reset_postdata();
        }
     }
     
     echo '</div>';
     echo $after_widget;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("affilwp_widget_block_widget");'));
?>