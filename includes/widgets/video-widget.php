<?php 
class affilwp_video_widget extends WP_Widget {

  // constructor
  function __construct() {
    parent::__construct(
    // Base ID of your widget
    'affilwp_video_widget', 

    // Widget name will appear in UI
    __('AffilWP video', 'affilwp'), 

    // Widget description
    array( 'description' => __( 'Display video in iframe.', 'affilwp' ), ) 
    );
  }

  // widget form creation
  function form($instance) { 
  // Check values 
  if( $instance) { 
       $title = esc_attr($instance['title']); 
       $video_url = esc_attr($instance['items']); // Added 
  } else { 
       $title = ''; 
       $video_url = ''; // Added 
  } 
  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('items'); ?>"><?php _e('Video URL:', 'affilwp'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('items'); ?>" name="<?php echo $this->get_field_name('items'); ?>" type="text" value="<?php echo $video_url; ?>" />
  </p>
  <?php }

  // update widget
  function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['items'] = strip_tags($new_instance['items']);
       return $instance;
  }

  // display widget
  function widget($args, $instance) {
     extract( $args );
     // these are the widget options
     $title = apply_filters('widget_title', $instance['title']);
     $video_url = $instance['items'];
     echo $before_widget;
     // Display the widget
     echo '<div>';

     // Check if title is set
     if ( $title ) {
        echo $before_title . $title . $after_title;
     }

     // Check if items is set
     if( $video_url ) {
        echo '
        <div class="video-box">
          <iframe width="300" height="149" src="'.$video_url.'" allowfullscreen></iframe>
        </div><!-- /.video-box -->
        ';
     }
     
     echo '</div>';
     echo $after_widget;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("affilwp_video_widget");'));
?>