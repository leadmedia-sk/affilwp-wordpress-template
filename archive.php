<?php get_header(); ?>

<main>
  <div class="container">
    <div class="content">
        <?php
        // Handle reviews first
        if (is_post_type_archive('reviews')):?>
          <?php get_template_part('sections/reviews'); ?>
      <?php else: ?>
          <?php // If not reviews, display a generic post type archive ?>
          <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part('sections/posts'); ?>

            <?php endwhile;

            // Previous/next page navigation.
            the_posts_pagination( array(
                'prev_text'          => __( 'Prechozia strana', 'affilwp' ),
                'next_text'          => __( 'Dalšia strana', 'affilwp' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Strana', 'affilwp' ) . ' </span>',
            ) ); ?>
            <?php else: ?>
            <div class="box box-offset">
              <p class="lead" style="margin-bottom:0;text-align: center;"><i class="fa fa-frown-o"></i> <?php _e('Zatiaľ sa tu nenachádzajú žiadné príspevky', 'affilwp') ?></p>
            </div>
          <?php endif; ?>
      <?php endif; ?>
    </div>

    <aside>
      <?php get_sidebar(); ?>
    </aside>
  </div><!-- /.container -->

  <a href="#top" class="scroll-top jsScrollLink"><i></i></a>
</main>

<?php get_footer(); ?>