<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />

  <meta name="robots" content="noodp" />
  <title><?php wp_title(); ?></title>
  <!--[if lt IE 9]>
  <script src="assets/js/html5shiv.js"></script>
  <script src="assets/js/respond.min.js"></script>
  <![endif]-->
  <!--[if gte IE 9]><style type="text/css">.gradient { filter: none; }</style><![endif]-->  
    
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <?php wp_head(); ?>   

  </head>

  <body <?php body_class() ?>> 
    <div class="page" id="top">
      <header>
        <div class="navbar navbar-default">
          <div class="container">
            <div class="navbar-header">
              <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                <img src="<?php echo get_option( 'site_logo' ); ?>" alt="<?php bloginfo('name'); ?>">
              </a>
              
              <?php if (get_option('site_headline_display')) :?>
                <div class="navbar-text">
                  <strong class="color"><?php bloginfo('name'); ?></strong>
                  <?php bloginfo('description'); ?>
                </div><!-- /.navbar-text -->
              <?php endif; ?>
              
              <form class="search-form" method="get" action="<?php echo home_url(); ?>" role="search">
                <input class="form-control" id="search-input" type="text" name="s" value="<?php the_search_query(); ?>" placeholder="<?php echo __('Search...', 'affilwp'); ?>" />
                <input id="searchsubmit" type="submit" class="search-btn" value="<?php _e("Search","affilwp"); ?>" />
              </form>
            </div>
          </div>

          <div class="navigation">
            <div class="container">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <nav id="navbar" class="navbar-collapse collapse">
                <?php 
                 if ( has_nav_menu( 'navbar-nav' ) ) {
                    wp_nav_menu( array(
                      'theme_location'  => 'navbar-nav',
                      'container'       => false,
                      'menu_class'      => 'nav nav-pills nav-justified',
                      'menu_id'         => '',
                      'fallback_cb'     => '',
                      )
                    ); 
                  }
                ?>
              </nav><!--/.nav-collapse -->
            </div><!-- /.container -->
          </div><!-- /.navigation -->
        </div><!-- /.navbar -->
      </header>

  












