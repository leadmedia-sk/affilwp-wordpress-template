<?php

/**
 * Template Name: Archive
 */
__( 'Archive', 'affilwp' );

get_header(); ?>

<?php 
  // ACF get_field
  $top_reviews_display = get_field('zobrazit_top_recenzie');
?>

<?php if ($top_reviews_display === true) :?>
  <?php get_template_part('sections/top-products'); ?>
<?php endif; ?>

<main>
  <div class="container">
    <div class="content">
      <?php get_template_part('sections/reviews'); ?>
    </div><!-- /.content -->

    <aside>
      <?php get_sidebar(); ?>
    </aside>
  </div><!-- /.container -->

  <a href="#top" class="scroll-top jsScrollLink"><i></i></a>
</main>
  
<?php get_footer(); ?>