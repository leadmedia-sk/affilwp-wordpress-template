<?php get_header(); ?>

<main>
  <div class="container">
    <div class="content">
      <?php get_template_part('sections/reviews'); ?>
    </div><!-- /.content -->

    <aside>
      <?php get_sidebar(); ?>
    </aside>
  </div><!-- /.container -->

  <a href="#top" class="scroll-top jsScrollLink"><i></i></a>
</main>
  
<?php get_footer(); ?>